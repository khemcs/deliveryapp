import { ApiServiceService } from 'src/app/api-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  res: any = []
  constructor(
    public router: ActivatedRoute,
    public api: ApiServiceService,
    public route: Router
  ) {
    this.setRestaurant()
  }

  async setRestaurant() {
    this.res = this.router.snapshot.paramMap.get('res');
    await localStorage.removeItem('key_restaurant')
    await this.api.setStore('key_restaurant', this.res)
    this.api.get("get_res/" + this.res).subscribe(async (res: any) => {
      // this.checkCartOfRestaurant(res)
      if (res.flag == false) {
        this.route.navigate(['notfoundRes-404'])
      } else {
        this.api.setStore('restaurant', res.data)
      }
    })
  }

  checkCartOfRestaurant(res){
    let store_id_res = this.api.getStore('restaurant').id_res_auto
    let id_res = res.data.id_res_auto
    if(store_id_res != id_res){
      this.api.setStore('cart',[])
    }
  }

}
