import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'notfoundRes-404',
    loadChildren: () => import('../not-found-restaurant/not-found-restaurant.module').then(m => m.NotFoundRestaurantPageModule)
  },
  {
    
    path: ':res',
    component: TabsPage,
    children: [
      {
        path: '',
        loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
      },
      {
        path: 'me',
        loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'delivery',
        loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
      },
      {

        path: 'map',
        loadChildren: () => import('../map/map.module').then(m => m.MapPageModule)
      },
      {
        path: 'delivery-order',
        loadChildren: () => import('../delivery-order/delivery-order.module').then(m => m.DeliveryOrderPageModule)
      },
      {
        path: 'restaurant-menu',
        loadChildren: () => import('../restaurant-menu/restaurant-menu.module').then(m => m.RestaurantMenuPageModule)
      },
      {
        path: 'order-in-menu',
        loadChildren: () => import('../order-in-cart/order-in-cart.module').then(m => m.OrderInCartPageModule)
      },
      // {
      //   path: 'delivery-status/:order_id',
      //   loadChildren: () => import('../delivery-status/delivery-status.module').then(m => m.DeliveryStatusPageModule)
      // },
      {
        path: 'booking',
        loadChildren: () => import('../tab4/tab4.module').then(m => m.Tab4PageModule)
      },
      {
        path: 'queue',
        loadChildren: () => import('../queue/queue.module').then(m => m.QueuePageModule)
      },
      {
        path: 'receive',
        loadChildren: () => import('../receive/receive.module').then(m => m.ReceivePageModule)
      },
      // {
      //   path: 'recovpassword',
      //   loadChildren: () => import('../recovpassword/recovpassword.module').then( m => m.RecovpasswordPageModule)
      // },
      {
        path: '',
        redirectTo: '/deltafood',
        pathMatch: 'full'
      }
    ],
  }
  ,
  {
    path: '',
    redirectTo: '/notfoundRes-404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
