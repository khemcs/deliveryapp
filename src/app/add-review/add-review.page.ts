import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database'
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.page.html',
  styleUrls: ['./add-review.page.scss'],
})
export class AddReviewPage implements OnInit {
  @Input() star = 1;
  @ViewChild('fileButton') fileButton

  imageURL: string
  desc: string
  // @ViewChild('fileButton') fileButton

  constructor(
    public modal: ModalController,
    public firebase: AngularFireDatabase,
    public http: HttpClient
  ) {
    console.log(this.star);
    console.log("constructor");


  }

  ngOnInit() {
    console.log(this.star);
  }


  close() {
    this.modal.dismiss();
  }
  add_review() {
    //test Add
    const desc = this.desc
    const image = this.imageURL
    


    let adddata = {}
       adddata['desc'] = desc,
    adddata['image'] = image,
   

    this.firebase.database.ref('users/posts/')
         
      .update(adddata).then((firebase:any)=>{        
           "desc ,image"    
           console.log("descDD",desc);  
       
       
        })
    //


    // this.close();
  }

  uploadFile() {
    this.fileButton.nativeElement.click()
  }


  public src: any = " "


  fileChanged(event) {

    const files = event.target
    const filest = event.target.files
    /////// 
    if (files.files && files.files[0]) {
      var reader = new FileReader();

      reader.onload = (e) => {
        this.src = e.target.result
        console.log(e.target.result);
      }
      reader.readAsDataURL(files.files[0]);
    }
    ///////////
    const data = new FormData()
    data.append('file', filest[0])
    data.append('UPLOADCARE_STORE', '1')
    data.append('UPLOADCARE_PUB_KEY', '806f9c121c0b275c8ae5')
    this.http.post('https://upload.uploadcare.com/base/', data)
      .subscribe((event: any) => {

        this.imageURL = event.file;
        console.log(event);
      })

  }

}
