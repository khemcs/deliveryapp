import { ApiServiceService } from 'src/app/api-service.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
declare var google;

@Component({
  selector: 'app-my-map',
  templateUrl: './my-map.component.html',
  styleUrls: ['./my-map.component.scss'],
})
export class MyMapComponent implements OnInit {
  @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;
  public latitude: any;
  public longitude: any;
  constructor(
    public modal: ModalController,
    private geolocation: Geolocation,
    public store: Storage,
    public ref: ChangeDetectorRef,
    public api: ApiServiceService
  ) {
    this.loadMap()
  }

  ngOnInit() { }

  makerlocation() {
    let pos = {
      lat: this.latitude,
      lng: this.longitude
    }
    this.close(pos, true)
  }

  close(data, con) {
    this.modal.dismiss(data, con)
  }

  posReady: boolean = false
  textPosInvalid : boolean = false
  loadMap() {
    this.posReady = false
    this.geolocation.getCurrentPosition().then((resp) => {
      let my_pos: any = this.api.getStore('my_pos')
      if (!my_pos) {
        this.latitude = resp.coords.latitude;
        this.longitude = resp.coords.longitude;
      } else {
        if (my_pos.lat == undefined || my_pos.lng == undefined) {
          this.latitude = resp.coords.latitude;
          this.longitude = resp.coords.longitude;
        } else {
          this.latitude = my_pos.lat
          this.longitude = my_pos.lng
        }
      }

      const pos = {
        lat: this.latitude,
        lng: this.longitude
      };
      const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
        center: pos,
        zoom: 17
      });
      map.setCenter(pos);
      const icon = {
        url: 'assets/icon/icon_map/u.png', // image url
        scaledSize: new google.maps.Size(50, 50), // scaled size
      };
      const marker = new google.maps.Marker({
        position: pos,
        map: map,
        title: 'Hello World!',
        icon: icon
      });
      map.addListener('idle', async (e) => {

        await marker.setPosition(map.getCenter());
        this.latitude = marker.getPosition().lat();
        this.longitude = marker.getPosition().lng();
        this.posReady = await true
      });

      setTimeout(() => {
        this.ref.detectChanges()
      }, 500);
    }).catch((error) => {
      console.log('Error getting location', error);
    })

    setTimeout(() => {
      this.textPosInvalid = true
    }, 6500);
  }

  mylocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;

      const pos = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
      const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
        center: pos,
        zoom: 17
      });
      map.setCenter(pos);
      const icon = {
        url: 'assets/icon/icon_map/u.png', // image url
        scaledSize: new google.maps.Size(50, 50), // scaled size
      };
      const marker = new google.maps.Marker({
        position: pos,
        map: map,
        title: 'Hello World!',
        icon: icon
      });
      map.addListener('idle', async (e) => {

        await marker.setPosition(map.getCenter());
        this.latitude = marker.getPosition().lat();
        this.longitude = marker.getPosition().lng();
        this.posReady = await true
      });

      setTimeout(() => {
        this.ref.detectChanges()
      }, 500);
    }).catch((error) => {
      console.log('Error getting location', error);
    })
  }

}
