import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecovpasswordPage } from './recovpassword.page';

const routes: Routes = [
  {
    path: '',
    component: RecovpasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecovpasswordPageRoutingModule {}
