import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecovpasswordPageRoutingModule } from './recovpassword-routing.module';

import { RecovpasswordPage } from './recovpassword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecovpasswordPageRoutingModule
  ],
  declarations: [RecovpasswordPage]
})
export class RecovpasswordPageModule {}
