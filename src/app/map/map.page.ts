import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { async } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from '../api-service.service';
declare var google;
@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;
  public latitude: any;
  public longitude: any;
  public user: any = []
  GGM: any;
  @Input() data: any = { location: '', send_options: {} };
  constructor(
    public api: ApiServiceService,
    public modal: ModalController,
    public router: Router,
    public rounte: ActivatedRoute,
    private geolocation: Geolocation,

    public store: Storage) {

  }

  ngOnInit() {
    // console.log(this.data);
    // this.route.queryParams.subscribe(params => {
    //   console.log(params);
    // });
    setTimeout(() => {
      this.loadMap();
      this.store.get('user').then((res: any) => {

        this.user = res
        if (res) {
          this.LoadMyLocation(this.user.mm_id)
        }

      });

    }, 1000);
  }

  public myLocation: any = []
  LoadMyLocation(mm_id) {
    this.api.get('LoadMyLocation/' + mm_id).subscribe(async (res: any) => {
      if (res) {
        this.myLocation = res
      }
    })
  }

  maker_location() {
    this.store.set('location_tmp', { lat: this.latitude, long: this.longitude, local_id: 0 })
    this.directions(this.latitude, this.longitude, 0, null);
  }

  select_location(local) {
    this.latitude = parseFloat(local.local_lat);
    this.longitude = parseFloat(local.local_long);
    this.directions(this.latitude, this.longitude, local.local_id, local.local_name)
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.store.get('location_tmp').then((local_data: any) => {
        if (local_data == null) {
          this.latitude = resp.coords.latitude;
          this.longitude = resp.coords.longitude;
        } else {
          if (local_data.lat == undefined || local_data.long == undefined) {
            this.latitude = resp.coords.latitude;
            this.longitude = resp.coords.longitude;
          } else {
            this.latitude = local_data.lat
            this.longitude = local_data.long
          }
        }
      }).then(() => {
        const pos = {
          lat: this.latitude,
          lng: this.longitude
        };
        const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
          center: pos,
          zoom: 17
        });
        map.setCenter(pos);
        const icon = {
          url: 'assets/icon/icon_map/u.png', // image url
          scaledSize: new google.maps.Size(50, 50), // scaled size
        };
        const marker = new google.maps.Marker({
          position: pos,
          map: map,
          title: 'Hello World!',
          icon: icon
        });
        map.addListener('idle', (e) => {

          marker.setPosition(map.getCenter());
          this.latitude = marker.getPosition().lat();
          this.longitude = marker.getPosition().lng();
          console.log(this.latitude, this.longitude);
        });
      })

    }).catch((error) => {
      console.log('Error getting location', error);
    })
  }

  directions(lat, lng, local_id, local_name) {
    console.log(lat, lng, local_id, local_name);
    this.GGM = new Object(google.maps);
    let directionsService = new this.GGM.DirectionsService();
    // var my_mapTypeId = this.GGM.MapTypeId.ROADMAP;
    let a = this.data.location.split(",");
    let my_Latlng = new this.GGM.LatLng(parseFloat(a[0]), parseFloat(a[1]));
    let initialTo = new this.GGM.LatLng(lat, lng);
    var request = {
      origin: my_Latlng,
      destination: initialTo,
      travelMode: this.GGM.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, (results, status) => {
      console.log(results.routes[0].legs[0]);
      var can_send = this.data.send_options.data
      var distance = parseInt(results.routes[0].legs[0].distance.value) / 1000
      var send: any = { text: 'ระยะทาง 0 กม. จัดส่งประมาณ 0 นาที', local_id: null, mark_name: null, price: 0, distance: 0, duration: 0, error: true }
      for (let cs of can_send) {
        if (distance <= cs.number) {
          this.store.set('location_tmp', { lat: lat, long: lng, local_id: local_id })
          send = {
            text: 'ระยะทาง ' + results.routes[0].legs[0].distance.text + ' จัดส่งประมาณ ' + results.routes[0].legs[0].duration.text,
            local_id: local_id,
            local_name: local_name,
            price: cs.price,
            distance: results.routes[0].legs[0].distance,
            duration: results.routes[0].legs[0].duration,
            error: false
          }
          break
        }
      }
      if (send.error) {
        this.api.Toast('เกินระยะจัดส่ง!')
      } else {
        this.modal.dismiss(send)
      }
    });
  }

  close() {
    // let res = this.route.snapshot.paramMap.get('res');
    // let res = this.rounte.snapshot._urlSegment.segments[0].path;
    // console.log(res);
    this.router.navigate(["../"]);
  }
}
