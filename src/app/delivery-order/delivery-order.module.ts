import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeliveryOrderPageRoutingModule } from './delivery-order-routing.module';

import { DeliveryOrderPage } from './delivery-order.page';
import { DeliveryStatusPage } from '../delivery-status/delivery-status.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DeliveryStatusItemComponent } from '../delivery-status/delivery-status-item/delivery-status-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliveryOrderPageRoutingModule
  ],
  declarations: [DeliveryOrderPage,DeliveryStatusPage,DeliveryStatusItemComponent],
  entryComponents:[DeliveryStatusPage,DeliveryStatusItemComponent],
  providers : [
    Geolocation
  ]
})
export class DeliveryOrderPageModule {}
