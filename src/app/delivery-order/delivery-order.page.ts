import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from '../api-service.service';
import { DeliveryStatusPage } from '../delivery-status/delivery-status.page';

@Component({
  selector: 'app-delivery-order',
  templateUrl: './delivery-order.page.html',
  styleUrls: ['./delivery-order.page.scss'],
})
export class DeliveryOrderPage implements OnInit {

  public user: any = {}
  constructor(public router: Router, private modalCtr: ModalController, public api: ApiServiceService, public store: Storage) {
    this.store.get('user').then((res: any) => {
      if (res) {
        this.user = res
        this.loadOrderDelivery()
      }
    })
  }

  ngOnInit() {

  }

  public order: any = []
  loadOrderDelivery() {
    this.api.get('loadOrderDelivery/' + this.user.mm_id).subscribe((res: any) => {
      this.order = res
    })
  }

  async routerRider(order) {
    await this.store.get('restaurant').then((res:any)=>{
      if(res){
        order.restaurant = res
      }
    })
    const modal = await this.modalCtr.create({
      component: DeliveryStatusPage,
      componentProps: { order: order }
    })

    return await modal.present()
  }

  close() {
    this.router.navigate(['../'])
  }
}
