import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OptPage } from './opt.page';

describe('OptPage', () => {
  let component: OptPage;
  let fixture: ComponentFixture<OptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
