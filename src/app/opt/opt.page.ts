import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-opt',
  templateUrl: './opt.page.html',
  styleUrls: ['./opt.page.scss'],
})
export class OptPage implements OnInit {
  @ViewChild('a') a;
  // @ViewChild('b') b;
  // @ViewChild('c') c;
  // @ViewChild('d') d;
  // @ViewChild('e') e;
  // @ViewChild('f') f;
  @Input() data: any = {
    code: ''
  };
value = '';
  constructor(public modal: ModalController,
    public alertController: AlertController,
    public api: ApiServiceService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.a.setFocus();
    }, 1000);

  }
  moveFocus(event, nextElement, previousElement) {
    if (nextElement == '') {
      this.confirm();
    }
    if (event.keyCode == 8 && previousElement) {
      previousElement.setFocus();
    } else if (event.keyCode >= 48 && event.keyCode <= 57) {
      if (nextElement) {
        nextElement.setFocus();
      }
    } else {
      event.path[0].value = '';
    }

  }
  close() {
    this.modal.dismiss({
      close_reg: false
    });
  }
  async success() {
    const alert = await this.alertController.create({
      cssClass: 'alert_css',
      header: 'ลงทะเบียนเรียบร้อยแล้ว',
      // subHeader: 'Subtitle',
      // message: 'This is an alert message.',
      buttons: ['OK']
    });
    alert.onDidDismiss().then((res) => {
      this.modal.dismiss({
        close_reg: true
      })
    });
    await alert.present();
  }
  async error() {
    const alert = await this.alertController.create({
      cssClass: 'alert_css',
      header: 'Error',
      subHeader: 'มีปัญหาในการบันทึกข้อมูล',
      // message: 'This is an alert message.',
      buttons: ['OK']
    });
    alert.onDidDismiss().then((res) => {
      // this.modal.dismiss({
      //   close_reg: true
      // })
    });
    await alert.present();
  }
  async not_otp() {
    const alert = await this.alertController.create({
      cssClass: 'alert_css',
      header: 'OTP ไม่ถูกต้อง',
      // subHeader: 'Subtitle',
      // message: 'This is an alert message.',
      buttons: ['OK']
    });
    alert.onDidDismiss().then((res) => {

    });
    await alert.present();
  }
  confirm() {
    // let val = this.a.value + this.b.value + this.c.value + this.d.value + this.e.value + this.f.value;
    // console.log(val);
    // this.value = val;
    console.log(this.data);
    if (this.value == this.data.otp) {
      this.api.post("API_user/add_user", this.data).subscribe((res: any) => {
        if (res == '1') {
          this.success();
        } else {
          this.error();
        }
        console.log(res);
      });
    } else {
      this.not_otp();
    }

  }
}
