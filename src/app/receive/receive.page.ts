import { ActivatedRoute, Router } from '@angular/router';
import { ChangeDetectorRef, Component } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ApiServiceService } from '../api-service.service';
import { Storage } from '@ionic/storage';
import { GroupPage } from '../group/group.page';
import { SendReviewComponent } from '../tab1/review/send-review/send-review.component';
declare var liff: any;
@Component({
  selector: 'app-queue',
  templateUrl: 'receive.page.html',
  styleUrls: ['receive.page.scss']
})
export class ReceivePage {
  cart: any = [];
  user: any = { mm_id: '' };
  public localhost = window.location.href.indexOf("localhost") != -1;
  public show = 0;
  public order: any = { delivery_contact: { c_name: '', c_phone: '' } };
  public payment = 0;
  data: any = {
    logo_url: '',
    name: '',
    address: '',
    id_res_auto: '',
    cover_url: '',
    line_settings: { liff_delivery: '' }
  };
  p_by_w: boolean = false
  public bank: any = [{ id_pay_type: 0 }];
  res = '';
  constructor(
    public modalCtrl: ModalController,
    public api: ApiServiceService,
    private modalCtr: ModalController,
    public router: Router,
    public modalController: ModalController,
    private ref: ChangeDetectorRef,
    public rounter: Router,
    public alert: AlertController
  ) {
    this.loadCart()
  }

  _restaurant: any = []
  ionViewDidEnter() {
    this._isSuccess = 0
    this.loadCart()
    this._restaurant = this.api.getStore('restaurant')
    setTimeout(() => {
      this.liff_init();
    }, 500);
  }

  _isSuccess: number = 0
  ionViewWillLeave() {
    this._isSuccess = 0
  }

  async loadCart() {
    this.cart = await this.api.getStore('cart');
    this.cfState1 = false
    if (this.cart.length > 0) {
      this.cfState1 = true
    }
    for (let c of this.cart) {
      if (c.p_by_w == '1') {
        this.p_by_w = true
        break
      }
    }
    // setTimeout(() => {
    // this.rounter.navigate(['/' + this.res + "/restaurant-menu" ]);  
    // }, 5000);



  }

  c_name: string = ''
  c_phone: string = ''
  setContact() {
    this.c_name = this.api.getStore('c_name') ? this.api.getStore('c_name') : this.user.mm_name
    this.c_phone = this.api.getStore('c_phone') ? this.api.getStore('c_phone') : ''
  }

  cooking() {
    let flag = false;
    this.order.items.forEach(element => {
      if (element.status != '0') {
        flag = true;
      }
    });
    return flag;
  }
  finish() {
    let flag = true;
    this.order.items.forEach(element => {
      if (element.status == '0' || element.status == '1') {
        flag = false;
      }
    });
    return flag;
  }

  ngOnInit(): void {

    this.load_data();

    setTimeout(() => {
      this.setContact()
    }, 1500);
  }
  async onReview() {
    const modal = await this.modalCtr.create({
      component: SendReviewComponent,
      backdropDismiss: true,
      cssClass: './review.component.scss'
    })

    modal.onWillDismiss().then((event: any) => {
      //   if (event.role == 'dismiss') {
      this.check_resive(this.user);
      //}
    });
    return await modal.present()
  }
  is_set_fb = false;
  check_resive(res) {
    this.api.get("check_receive/" + res.mm_id + '/' + this.data.id_res_auto).subscribe((res1: any) => {
      if (res1.flag) {
        this.order = res1.data;

        for (let c of this.order.items) {
          this.p_by_w = false
          if (c.p_by_w == '1') {
            this.p_by_w = true
            break
          }
        }
        if (!this.is_set_fb) {
          this.is_set_fb = true;
          this.api.fb_get(this.data.id_res_auto + "_" + this.data.code + "/orders/" + this.order.order_id, (va) => {
            this.check_resive(res);
          });
        }
        if (this.order.status == '5') {
          setTimeout(() => {
            this.ref.detectChanges();
          }, 1000);
        }
        if (this.order.status == '1') {
          setTimeout(() => {
            if (this._isSuccess < 1) {
              // this.onReview();
              this._isSuccess++
            }
          }, 1000);
        }
        this.show = 2;
      } else {
        this.show = 1;
        // this.api.setStore("cart", []);
      }
      this.ref.detectChanges();
    })
  }

  async cancelOrder() {
    const alert = await this.alert.create({
      mode: 'ios',
      cssClass: 'my-custom-class',
      header: 'ยกเลิกการสั่ง?',
      message: 'กด "ยืนยัน" หากต้องการยกเลิกการทำรายการ',
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel'
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.api.post('cancelOrder', { order_id: this.order.order_id }).subscribe((res: any) => {
              if (res.status == 'success') {
                this.api.fb_setAuto(this.data.id_res_auto + "_" + this.data.code + "/data_pay");
                // this.api.fb_setManual(this.data.id_res_auto + "_" + this.data.code + "/new_delivery", this.order.order_id);
                this.loadCart()
                this.check_resive(this.user);
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }
  async liff_init() {
    await liff.init({ liffId: this.data.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0h1u1NlG3PbloMN0J_R1YRDTByYDd7GWgSdFFxaX5nYjohDipeZAMnbiE_YGsiByEEMQJyPCs1M2Ih",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U84a429168faac5ff5d331821944253bb"
      };
      this.api.post("update_profile", data).subscribe((res) => {
        console.log(res);
        this.user = res;
        this.api.fb_get(this.data.id_res_auto + "_" + this.data.code + "/data_pay", () => {
          this.check_resive(res);
        });
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {
      console.log("liff.isInClient() || liff.isLoggedIn()");
      this.getUserProfile();
    } else {
      // liff.login({ redirectUri: window.location.href })
    }
  }
  async getUserProfile() {
    const profile = await liff.getProfile();
    this.api.post("update_profile", profile).subscribe((res) => {
      this.user = res;
      this.api.fb_get(this.data.id_res_auto + "_" + this.data.code + "/data_pay", () => {
        this.check_resive(res);
      });

    });

  }
  /////////////////////////////////////////////////////////// load defult ///////////////////////////////////////////////////////////

  load_data() {
    let url = this.router['routerState'].snapshot.url;
    let urls = url.split("/");
    this.res = urls[1];

    this.api.get("get_res/" + this.res).subscribe((res: any) => {
      this.data = res.data;
      console.log(this.data);
      this.liff_init().then(() => {
        this.loadDeliveryService()
      });

    });

  }

  /////////////////////////////////////////////////////////// รายการที่สั่ง ///////////////////////////////////////////////////////////
  public cfState1: boolean = false
  add_count(item) {
    item.count++;
    this.api.setStore("cart", this.cart)
    this.cfState1 = true
  }

  sum_all() {
    if (this.order.sum_price) {
      return this.order.sum_price;
    } else {
      return 0;
    }

  }
  sum_all_cart() {

    let sum = 0;
    this.cart.forEach(element => {
      sum += this.get_price(element);
    });
    return sum;
  }

  count_all() {
    let count = 0;
    this.cart.forEach((val) => {
      count += val.count;
    });
    return count;
  }

  async remove(index) {
    await this.cart.splice(index, 1);
    await this.api.setStore("cart", this.cart)
    let cart = this.api.getStore('cart')
    this.cfState1 = false
    if (cart.length > 0) {
      this.cfState1 = true
    } else {
      // this.isCart(cart)
    }

  }
  remove_count(item, i) {
    if (item.count > 1) {
      item.count--;
      this.api.setStore("cart", this.cart)
    } else {
      this.remove(i);
    }
  }

  async add_food() {
    const modal = await this.modalController.create({
      component: GroupPage,
      cssClass: 'my-custom-class',
      componentProps: {
        id_res_auto: this.data.id_res_auto
      }
    });
    modal.onDidDismiss().then((data: any) => {
      // this.s.get('cart').then((food: any) => {
      //   if (data) {
      //     this.cart = food;
      //     this.cfState1 = true
      //   }
      // });
    });
    return await modal.present();
  }

  get_html(c): String {
    let out: String = '';
    // console.log(c.details);
    for (let entry of c.details) {
      // let price = 0;
      let temp = '';
      let flag = true;
      for (let sub of entry.sub) {
        if (sub.selected) {
          if (flag) {
            flag = false;
            temp += sub.name[0].title;
          } else {
            temp += ", " + sub.name[0].title;
          }
        }
      }
      if (temp != '') {
        out += entry.name[0].title + " : ";
        out += temp;
        out += "<br>";
      }

    }

    for (let entry of c.toppings) {
      if (entry.count - 0 > 0) {
        out += "  [" + entry.count + "] " + entry.title + " (";
        if (entry.price > 0) {
          out += "+";
        }
        out += (entry.price * entry.count) + ")";
        out += "<br>";
      }
    }
    if (c.comments.trim() != '') {
      out += "Comment : " + c.comments;
    }

    return out;
  }
  get_price(c): number {
    let out = 0;
    if (c.is_buffet - 0 == 1) {
      out = 0;
    } else {
      out = parseFloat(c.price);
    }

    for (let entry of c.details) {
      for (let sub of entry.sub) {
        if (sub.selected) {
          out += parseFloat(sub.price);
        }
      }
    }
    for (let entry of c.toppings) {
      if (Number(entry.count) > 0) {
        out += parseFloat(entry.price) * Number(entry.count);
      }
    }
    return out * Number(c.count);
  }


  /////////////////////////////////////////////////////////// สถานที่ส่ง ///////////////////////////////////////////////////////////
  public takeHome = 'res'
  public spoon = false
  getSpoon() {
    if (this.spoon) {
      this.spoon = false
    } else {
      this.spoon = true
    }
  }

  _list_ds: any = []
  loadDeliveryService() {
    this._list_ds = []
    this.api.get('loadDeliveryService/' + this.data.id_res_auto).subscribe((res: any) => {
      if (res) {
        res.forEach((el) => {
          let obj = { title: el, checked: false }
          this._list_ds.push(obj)
        });
      }
    })
  }

  getService(e, i) {
    e = e.detail.checked
    this._list_ds.forEach((el, index) => {
      if (index == i) {
        el.checked = e
      }
    });
  }
  /////////////////////////////////////////////////////////// ยอดชำระ ///////////////////////////////////////////////////////////
  public cfState2: boolean = true
  public pay: any = { type: 0, id_pay_type: undefined }
  paymentSelect(e) {
    let type = e.detail.value
    this.cfState2 = true
    this.src = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
    this.pay = { type: 0, id_pay_type: undefined }
    if (type == 1) {
      this.pay = { type: 1, id_pay_type: this.bank[0].id_pay_type }
      this.cfState2 = false
    }
  }

  bankSelect(e) {
    this.pay = {
      type: 1,
      id_pay_type: e.detail.value
    }
  }

  public src: any = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
  changeFile(e) {
    var input = e.target
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (el) => {
        this.src = {
          path: el.target.result,
          name: input.files[0].name,
          state: true
        }
        this.cfState2 = true
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    } else {
      this.src = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
      this.cfState2 = false
    }
  }

  async error() {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      message: 'ไม่สามารถส่งข้อมูลได้',
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          },
        },
      ],
    });

    await alert.present();
  }
  async confirmPayment() {
    const alert = await this.alert.create({
      mode: 'ios',
      cssClass: 'my-custom-class',
      header: 'ยืนยัน!',
      message: 'ยืนยันชำระเงินถูกต้อง',
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel'
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            var valid = this.validatorState()
            if (valid.val) {
              let order = {
                res: this.data.id_res_auto,
                order_id: this.order.order_id,
                payType: this.pay,
                slip: this.src.path,
                total: this.sum_all()
              }
              this.api.post('receiveYourself_payment', order).subscribe((res: any) => {
                if (res.status == 'success') {
                  this.cart = [];
                  // this.s.set("cart", this.cart);
                  this.api.setStore("cart", [])
                  console.log('success');
                  this.api.fb_setAuto(this.data.id_res_auto + "_" + this.data.code + "/data_pay");
                  this.api.fb_setManual(this.data.id_res_auto + "_" + this.data.code + "/add_receive_payment", res.order_id);
                } else {
                  console.log('fail');
                  this.error();

                }
              })
            } else {
              setTimeout(async () => {
                const alertFail = await this.alert.create({
                  mode: 'ios',
                  cssClass: 'my-custom-class',
                  header: 'ผิดผลาด!',
                  message: valid.txt,
                  buttons: [
                    {
                      text: 'ตกลง'
                    }
                  ]
                });
                await alertFail.present();
              }, 0);
            }
          }
        }
      ]
    });
    await alert.present();
  }
  async confirmOrder() {
    const alert = await this.alert.create({
      mode: 'ios',
      cssClass: 'my-custom-class',
      header: 'ยืนยัน!',
      message: 'ยืนยันการสั่ง.',
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel'
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            var valid = this.validatorState()
            // if (valid.val) {
            this.setOrder()
            // } else {
            //   setTimeout(async () => {
            //     const alertFail = await this.alert.create({
            //       mode: 'ios',
            //       cssClass: 'my-custom-class',
            //       header: 'ผิดผลาด!',
            //       message: valid.txt,
            //       buttons: [
            //         {
            //           text: 'ตกลง'
            //         }
            //       ]
            //     });
            //     await alertFail.present();
            //   }, 0);
            // }
          }
        }
      ]
    });
    await alert.present();
  }

  validatorState() {
    var data = [
      // { val: this.cfState1, txt: 'โปรดเพิ่มรายการอาหาร' },
      { val: this.cfState2, txt: 'โปรดแนบหลักฐานการโอน' },
      // ...n
    ]
    var valid = { val: true, txt: '' }
    for (let el of data) {
      if (!el.val) {
        valid = el
        break
      }
    }
    return valid
  }

  setOrder() {
    var tmp_s = []
    this._list_ds.forEach(el => {
      if (el.checked) {
        tmp_s.push(el.title)
      }
    });

    let order = {
      res: this.data.id_res_auto,
      user: this.user.mm_id,
      order: this.cart,
      delivery_service: tmp_s,
      takeHome: this.takeHome,
      spoon: this.spoon,
      payType: this.pay,
      slip: this.src.path,
      delivery_contact: { c_name: this.c_name, c_phone: this.c_phone }
    }
    this.api.post('receiveYourself', order).subscribe((res: any) => {
      if (res) {
        this.cart = [];
        // this.s.set("cart", this.cart);
        this.api.setStore("cart", []);
        console.log('success');
        this.api.fb_setAuto(this.data.id_res_auto + "_" + this.data.code + "/data_pay");
        this.api.fb_setManual(this.data.id_res_auto + "_" + this.data.code + "/new_receive", res.order_id);
      } else {
        console.log('fail');
      }
    })
  }
}
