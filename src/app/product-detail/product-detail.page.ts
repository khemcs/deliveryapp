import { Component, OnInit } from '@angular/core';
import { NavParams, NavController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  public temp;
  public cart_count = 0;
  public counts = [
    { number: 0 },
    { number: 1 },
    { number: 2 },
    { number: 3 },
    { number: 4 },
    { number: 5 },
    { number: 6 },
    { number: 7 },
    { number: 8 },
    { number: 9 }
  ];
  public currency = "บาท";
  public currency_shot = '฿';
  public data: any = { count: 1, name: '', details: [], toppings: [], comments: '', is_buffet: '0' };
  public lang = 'th';
  public index = -1;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private s: Storage,
    public modalCtrl: ModalController,
    private api: ApiServiceService
  ) {
    this.setFoodData()
    // s.get("data").then((data: any) => {
    //   this.currency = data.currency_full;
    //   // this.currency_shot = data.currency_shot;
    // })
  }
  ngOnInit() {
  }
  close() {
    this.modalCtrl.dismiss();
  }

  setFoodData() {
    let food = this.navParams.get("data")
    food.count = 1
    this.api.setStore('tmp_food_detail', food)
    setTimeout(() => {
      this.data = this.api.getStore('tmp_food_detail')

      // this.index = this.navParams.get('index');

      this.s.get('AppLangcode').then(lang => {
        this.lang = lang;
      });
    }, 700);
  }


  get_price_sub(del) {
    var price = 0;
    for (let val of del.sub) {
      if (val.selected) {
        price += val.price - 0;
      }
    }
    return price;
  };
  sum_item = function (f) {

    var total_price = f.price - 0;
    if (f.is_buffet - 0 == 1) {
      total_price = 0;
    }
    for (let val of f.toppings) {
      total_price += (val.price - 0) * val.count;
    };
    for (let detail of f.details) {
      total_price += this.get_price_sub(detail);
    };
    return total_price * f.count;
  };
  check_true(index) {
  }
  t = null;
  selected_change(index, data, d, j) {
    if (this.t !== null) {
      clearTimeout(this.t);
    }

    if (d.number_min - 0 === 1 && d.number_max - 0 === 1) {
      for (let i = 0; i < data.details[index].sub.length; i++) {
        data.details[index].sub[i].selected = false;
      }
      data.details[index].sub[j].selected = true;
    } else {
      data.details[index].sub[j].selected = !data.details[index].sub[j].selected;

      this.t = setTimeout(function () {
        let count_true = 0;
        for (let val of data.details[index].sub) {
          if (val.selected) {
            count_true++;
          }
        }
        if (count_true > data.details[index].number_max) {
          let c = count_true - data.details[index].number_max;
          for (let i = data.details[index].sub.length - 1; i >= 0; i--) {
            if (data.details[index].sub[i].selected) {
              data.details[index].sub[i].selected = false;
              c--;
              if (c === 0) {
                break;
              }
            }
          }
        } else if (count_true < data.details[index].number_min) {
          let c = data.details[index].number_min - count_true;
          for (let i = 0; i < data.details[index].sub.length; i++) {
            if (!data.details[index].sub[i].selected) {
              data.details[index].sub[i].selected = true;
              c--;
              if (c === 0) {
                break;
              }
            }
          }
        }
      }, 1500);
    }
  }
  ionViewDidLoad() {
  }

  addtopping(t) {
    t.count++
    // this.tping.tcount++;

  }
  removetopping(t) {
    if (t.count > 0) {
      t.count--;

    }
  }
  add() {
    this.data.count += 1
    // this.data.countPick++;
  }
  remove() {
    // if (this.data.countPick > 1) {
    //   this.data.countPick--;
    // }
    if (this.data.count > 1) {
      this.data.count--;

    }
  }

  array_lang(text) {
    for (let val of text) {
      if (val.shot) {
        if (val.shot === this.lang) {
          return val.title;
        }
      }
    }
    return text[0].title;
  }

  add_product(data) {
    if (this.index == -2) {
      console.log(data);
      this.api.post("change_item", data).subscribe((res) => {
        console.log(data);
        // this.api.fb_set("data_pay");
        this.modalCtrl.dismiss();
      });
    } else {
      if (data.status - 0 === 0) {
        // this.translate.get("oos_text").subscribe(oos_text => {
        this.api.Toast("Out of stock");
        // });
      } else {
        var temp = [];
        let cart = this.api.getStore('cart')
        setTimeout(() => {
          if (!cart) { cart = []; }
          temp = cart;
          if (this.index == -1) {
            let a = this.check_food(data.f_id, temp);
            if (a.index === -1) {
              // data.count = 1;
              temp.push(data);
            } else {
              temp[a.index].count = a.count;
            }
            let count = 0;
            for (let i = 0; i < temp.length; i++) {
              count += temp[i].count - 0;
            }
            this.cart_count = count;
            this.api.setStore('cart', temp);
            // this.translate.get("add").subscribe(add => {
            //   this.translate.get("success").subscribe(success => {
            this.api.Toast('Add ' + data.name + " " + 'SUccess');
            this.close();
            // });
            // });
          } else {
            temp[this.index] = data;
            this.api.setStore('cart', temp);
            this.close();
          }

        }, 500);
      }
    }
  }
  is_change(food) {
    var change = false;
    if (food.toppings.length - 0 !== 0) {
      let count = 0;
      for (let i = 0; i < food.toppings.length; i++) {
        count += food.toppings[i].count - 0;
      }
      if (count !== 0) {
        return true;
      }
    }

    for (let key = 0; key < food.details.length; key++) {
      let val = food.details[key];
      for (let key2 = 0; key2 < val.sub.length; key2++) {
        let val2 = val.sub[key2];
        // console.log(val2.default_val , val2.selected);

        if (val2.default_val && !val2.selected) {
          change = true;
        } else if (!val2.default_val && val2.selected) {
          change = true;
        }
      };
    };
    console.log(food.comments);

    if (food.comments !== "") {
      change = true;
    }
    return change;
  };
  check_food(f_id, val) {
    let out = { count: 1, index: -1 };
    for (let key = 0; key < val.length; key++) {
      if (val[key].f_id - 0 === f_id - 0) {
        if (!this.is_change(val[key])) {
          out.count = val[key].count - 0 + this.data.count;
          out.index = key;
        }
      }
    }
    return out;
  };
  onSelectChange(index1) {
  }

  addFood(data) {
    let cart = this.api.getStore('cart') ? this.api.getStore('cart') : []
    if (cart.length < 1) {
      cart.push(data)
      this.api.setStore('cart', cart)
      this.close();
    } else {
      let foodChange = 0
      let same = 0
      let tmpCart = []
      cart.forEach((el, index) => {
        if (data.f_id == el.f_id) {
          let tmp_opt = JSON.stringify(el.details)
          let opt = JSON.stringify(data.details)
          let tmp_tp = JSON.stringify(el.toppings)
          let tp = JSON.stringify(data.toppings)
          if (tmp_opt + tmp_tp == opt + tp && data.comments == el.comments) {
            el.count = el.count - 0 + data.count;
            same++
          } else {
            foodChange++
          }
        } else {
          let tmp_opt = JSON.stringify(el.details)
          let opt = JSON.stringify(data.details)
          if (tmp_opt == opt) {
            el.count = el.count - 0 + data.count;
            same++
          } else {
            foodChange++
          }
        }
      });

      if (same > 0) {
        foodChange = 0
      }

      if (foodChange > 0) {
        cart.forEach(el => {
          tmpCart.push(el)
        })
        tmpCart.push(data)
        this.api.setStore('cart', tmpCart)
      } else {
        this.api.setStore('cart', cart)
      }
      this.close();
    }
  }
}
