import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductDetailPageRoutingModule } from './product-detail-routing.module';

import { ProductDetailPage } from './product-detail.page';
// import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    // TranslateModule,
    ReactiveFormsModule,
    IonicModule,
    ProductDetailPageRoutingModule
  ],
  declarations: [
    // ProductDetailPage
  ],schemas:[NO_ERRORS_SCHEMA]
})
export class ProductDetailPageModule {}
