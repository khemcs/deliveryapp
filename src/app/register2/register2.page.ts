import { OptPage } from './../opt/opt.page';
import { AlertController, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-register2',
  templateUrl: './register2.page.html',
  styleUrls: ['./register2.page.scss'],
})
export class Register2Page implements OnInit {
  myForm: FormGroup;
  regis = {
    phone: '',
    name: '',
    email: ''
  }
  constructor(
    public modalCtrl: ModalController,
    public formBuilder: FormBuilder,
    public api: ApiServiceService,
    public alertController: AlertController
  ) {
    this.myForm = new FormGroup({
      name: new FormControl("", Validators.compose([
        Validators.required,
        Validators.maxLength(50),
        Validators.minLength(5)
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.minLength(10),
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
      ])),
      email: new FormControl("", Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ]))

    })
    // this.registerForm.setValue({
    //   phone: 'aaaaaaa',
    //   name: 'bbbb',
    //   email: 'ccc'
    // });
  }
  ngOnInit() {



  }
  async otp(code) {

    let data = this.myForm.value;
    data.code = code.code;
    data.otp = code.otp;
    const modal = await this.modalCtrl.create({
      component: OptPage,
      componentProps: { data: data },
      backdropDismiss: false,
      cssClass: 'regis_modal',
    });

    modal.onWillDismiss().then((event: any) => {
      console.log(event);
      if (event.data.close_reg) {
        setTimeout(() => {
          this.close();
        }, 1000);

      }
    })

    return await modal.present();
  }
  async wait_timeout() {
    const alert = await this.alertController.create({
      cssClass: 'alert_css',
      header: 'รออีกอย่างน้อย 3 นาทีถึงจะส่ง OTP ได้อีกครั้ง',
      // subHeader: 'Subtitle',
      // message: 'This is an alert message.',
      buttons: ['OK']
    });
    alert.onDidDismiss().then((res) => {

    });
    await alert.present();
  }
  async has_number_phone() {
    const alert = await this.alertController.create({
      cssClass: 'alert_css',
      header: 'เบอร์นี้เคยลงทะเบียนแล้ว',
      // subHeader: 'Subtitle',
      // message: 'This is an alert message.',
      buttons: ['OK']
    });
    alert.onDidDismiss().then((res) => {
      this.close();
    });
    await alert.present();
  }
  register() {
    console.log(this.myForm);
    this.api.post('checkRegisterUser', { phone: this.myForm.value.phone }).subscribe((res: any) => {
      console.log(res);
      if (res.text == "has_number_phone") {
        this.has_number_phone();
      } else if (res.text == 'can_register') {
        if (res.event.txt == "wait_timeout") {
          this.wait_timeout();
        } else {
          this.otp(res.event);

        }

      }

    })

  }
  close() {
    console.log("close");
    this.modalCtrl.dismiss();

  }
}
