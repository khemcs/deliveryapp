import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderInCartPage } from './order-in-cart.page';

describe('OrderInCartPage', () => {
  let component: OrderInCartPage;
  let fixture: ComponentFixture<OrderInCartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderInCartPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderInCartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
