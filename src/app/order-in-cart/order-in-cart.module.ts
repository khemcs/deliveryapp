import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderInCartPageRoutingModule } from './order-in-cart-routing.module';

import { OrderInCartPage } from './order-in-cart.page';
import { Geolocation } from '@ionic-native/geolocation';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderInCartPageRoutingModule,
  ],
  declarations: [OrderInCartPage],
  providers: [
    Geolocation
  ],
})
export class OrderInCartPageModule { }
