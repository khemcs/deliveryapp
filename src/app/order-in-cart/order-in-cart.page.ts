import { SendReviewComponent } from './../tab1/review/send-review/send-review.component';
import { Router } from '@angular/router';
import { ModalController, AlertController, ToastController } from '@ionic/angular';
import { ApiServiceService } from './../api-service.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MyMapComponent } from '../my-map/my-map.component';
import { Geolocation } from '@ionic-native/geolocation';
declare var google;
declare var liff: any;

@Component({
  selector: 'app-order-in-cart',
  templateUrl: './order-in-cart.page.html',
  styleUrls: ['./order-in-cart.page.scss'],
})
export class OrderInCartPage implements OnInit {

  constructor(
    public api: ApiServiceService,
    public modalCtrl: ModalController,
    private geolocation: Geolocation,
    public alert: AlertController,
    public route: Router,
    public toast: ToastController,
    public deRef: ChangeDetectorRef
  ) { }

  ngOnInit() {

  }

  _restaurant: any = []
  ionViewDidEnter() {
    this.loadCart()
    let my_pos = this.api.getStore('my_pos')
    this.directions(my_pos.lat, my_pos.lng, 0, null)
    this._restaurant = this.api.getStore('restaurant')

    if (this._restaurant.bank.length > 0) {
      this._transferBank = this._restaurant.bank[0].id_pay_type
    }

    setTimeout(() => {
      this.liff_init();
    }, 500);
  }

  _cart: any = []
  loadCart() {
    this._cart = this.api.getStore('cart')
  }

  public localhost = window.location.href.indexOf("localhost") != -1;
  async liff_init() {
    await liff.init({ liffId: this._restaurant.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f"
      };
      this.api.post("update_profile", data).subscribe(async (res) => {
        this.user = await res;
        // this.check_delivery();
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {
      await this.getUserProfile();
    } else {
      // liff.login({ redirectUri: window.location.href })
    }
  }

  async getUserProfile() {
    const profile = await liff.getProfile();
    this.api.post("update_profile", profile).subscribe((res) => {
      this.user = res;
      // this.check_delivery();
    });
  }

  sumPriceByRec(food) {
    let sum = 0
    sum = (food.countPick * food.price) + sum
    return sum
  }

  async btnCalc(food, index, con) {
    let cart = await this.api.getStore('cart')
    let temp: any = await []
    await cart.forEach(async (el, i) => {
      if (index == i) {
        if (con == 'add') {
          el.countPick = food.countPick + 1
        } else {
          el.countPick = food.countPick - 1
        }
      }


      if (el.countPick > 0) {
        temp.push(el)

      }
    });
    await this.api.setStore('cart', temp)
    await this.loadCart()

    if (this._cart.length < 1) {
      this.route.navigate([this._restaurant.uri + '/restaurant-menu'])
    }
  }

  async editMap() {
    const modal = await this.modalCtrl.create({
      component: MyMapComponent,
      componentProps: {
        data: ''
      },
      backdropDismiss: false,
      cssClass: 'location_page',
    });

    modal.onWillDismiss().then(async (e: any) => {
      if (e.role) {
        await this.api.setStore('my_pos', e.data)
        let my_pos = await this.api.getStore('my_pos')
        await this.directions(my_pos.lat, my_pos.lng, 0, null)
        this._location.price = await e.price
        this._location.text = await e.text
        this._location.distance = await e.distance
        this._location.duration = await e.duration
        // this.newLocation = e.data.local_name

      }
    })
    return await modal.present();
  }

  GGM: any;
  _location: any = { local_name: '-', text: '-', newLocation: '' }
  directions(lat, lng, local_id, local_name) {
    let data_res = this.api.getStore('restaurant')
    this.GGM = new Object(google.maps);
    let directionsService = new this.GGM.DirectionsService();
    // var my_mapTypeId = this.GGM.MapTypeId.ROADMAP;
    let a = data_res.location.split(",");
    let my_Latlng = new this.GGM.LatLng(parseFloat(a[0]), parseFloat(a[1]));
    let initialTo = new this.GGM.LatLng(lat, lng);
    var request = {
      origin: my_Latlng,
      destination: initialTo,
      travelMode: this.GGM.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, (results, status) => {
      console.log(results.routes[0].legs[0]);
      var can_send = data_res.send_options.data
      var distance = parseInt(results.routes[0].legs[0].distance.value) / 1000
      var send: any = { text: 'ระยะทาง 0 กม. จัดส่งประมาณ 0 นาที', local_id: null, mark_name: null, price: 0, distance: 0, duration: 0, error: true }
      for (let cs of can_send) {
        if (distance <= cs.number) {
          this.api.setStore('location_tmp', { lat: lat, long: lng, local_id: local_id })
          send = {
            text: 'ระยะทาง ' + results.routes[0].legs[0].distance.text + ' จัดส่งประมาณ ' + results.routes[0].legs[0].duration.text,
            local_id: local_id,
            local_name: results.routes[0].legs[0].end_address,
            end_address: results.routes[0].legs[0].end_address,
            price: cs.price,
            distance: results.routes[0].legs[0].distance,
            duration: results.routes[0].legs[0].duration,
            lat: lat,
            long: lng,
            error: false,
            newLocation: this._location.newLocation
          }
          break
        }
      }
      if (send.error) {
        this.api.Toast('เกินระยะจัดส่ง!')
      } else {
        // this.modalCtrl.dismiss(send)
        this._location = send
      }
    });
  }

  totalPriceFood() {
    let total = 0
    let cart = this.api.getStore('cart')
    cart.forEach(el => {
      total += el.price * el.countPick
    });
  }

  totalAllPrice(con) {
    let deliveryP = this._location.price
    let foodsP = this._cart
    let total = 0
    foodsP.forEach(el => {
      total += el.countPick * el.price
    });
    if (con == 'all') {
      total += deliveryP
    }
    return total
  }

  _stateTranfer: boolean = false
  typeTransfer(e) {
    this._stateTranfer = false
    if (e.detail.value - 0 === 1) {
      this._stateTranfer = true
    }
  }

  public src: any = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
  changeFile(e) {
    var input = e.target
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (el) => {
        this.src = {
          path: el.target.result,
          name: input.files[0].name,
          state: true
        }
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    } else {
      this.src = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
    }
  }

  async confirmOrder() {
    if (this._transferType == 1 && this.src.path == '') {
      const toast = await this.toast.create({
        header: 'ผิดผลาด!',
        message: 'โปรดแนบหลักฐานการโอน',
        position: 'top',
        buttons: [
          {
            text: 'ตกลง',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      await toast.present();
    } else {
      const alert = await this.alert.create({
        mode: 'ios',
        cssClass: 'my-custom-class',
        header: 'ยืนยัน!',
        message: 'ยืนยันการสั่ง.',
        buttons: [
          {
            text: 'ปิด',
            role: 'cancel'
          },
          {
            text: 'ยืนยัน',
            handler: () => {
              this.saveOrder()
            }
          }
        ]
      });
      await alert.present();
    }
  }

  _getSpoon: boolean = true
  _transferType: number = 0
  _transferBank: number = 0
  user: any = []
  saveOrder() {
    var order: any = {
      user: this.user,
      res: this._restaurant.id_res_auto,
      cart: this._cart,
      spoon: this._getSpoon,
      local: this._location,
      payType: { type: this._transferType, id_pay_type: this._transferBank },
      slip: this.src.path,
      total: this.totalAllPrice('all')
    }
    this.api.post('addOrder', order).subscribe(async (res: any) => {
      if (res.status == 'success') {
        await this.api.fb_setAuto(this._restaurant.id_res_auto + "_" + this._restaurant.code + "/data_pay");
        await this.api.fb_setManual(this._restaurant.id_res_auto + "_" + this._restaurant.code + "/new_delivery", res.order_id);
        await this.setDefult();
      }
    })
  }

  async setDefult() {
    await this.api.setStore('cart', [])
    this._getSpoon = await true
    this._location = await { local_name: '-', text: '-', newLocation: '' }
    this.src.path = await ''
    this.route.navigate([this._restaurant.uri + '/restaurant-menu'])
  }

}
