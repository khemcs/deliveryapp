import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderInCartPage } from './order-in-cart.page';

const routes: Routes = [
  {
    path: '',
    component: OrderInCartPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderInCartPageRoutingModule {}
