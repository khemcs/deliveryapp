import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ProductDetailPage } from '../product-detail/product-detail.page';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
})
export class ProductListPage implements OnInit {
  public fg_id = 0;
  public name = '';
  public items = [];
  public cart = [];
  public cart_count = 0;
  constructor(public modalCtrl: ModalController,
    public api: ApiServiceService,
    public changeDetector: ChangeDetectorRef,
    public modalController: ModalController,
    private s: Storage
    , public NavParams: NavParams) {
    this.fg_id = this.NavParams.get("fg_id");
    this.name = this.NavParams.get("name");
    setInterval(() => {
      this.s.get('cart').then((v) => {
        if (!v) { v = []; }
        var temp = v;
        let count = 0;
        for (let i = 0; i < temp.length; i++) {
          count += temp[i].count - 0;
        }
        this.cart_count = count;
      });
    }, 1000);
  }


  select_item(item) {
    console.log(item);

  }
  count_cart(f_id) {
    let count = 0;
    for (let i = 0; i < this.cart.length; i++) {
      if (this.cart[i].f_id == f_id) {
        count += this.cart[i].count - 0;
      }
    }
    return count;
  }
  is_change(food) {
    var change = false;
    if (food.toppings.length - 0 !== 0) {
      let count = 0;
      for (let i = 0; i < food.toppings.length; i++) {
        count += food.toppings[i].count - 0;
      }
      if (count !== 0) {
        return true;
      }
    }

    for (let key = 0; key < food.details.length; key++) {
      let val = food.details[key];
      for (let key2 = 0; key2 < val.sub.length; key2++) {
        let val2 = val.sub[key2];
        if (val2.default_val && !val2.selected) {
          change = true;
        } else if (!val2.default_val && val2.selected) {
          change = true;
        }
      };
    };
    if (food.comments !== "") {
      change = true;
    }
    return change;
  };
  check_food(f_id, val) {
    let out = { count: 1, index: -1 };
    for (let key = 0; key < val.length; key++) {
      if (val[key].f_id - 0 === f_id - 0) {
        if (!this.is_change(val[key])) {
          out.count = val[key].count - 0 + 1;
          out.index = key;
        }
      }
    }
    return out;
  };
  add_product(data) {
    var temp = [];
    this.s.get('cart').then((v) => {
      if (!v) { v = []; }
      temp = v;
      let a = this.check_food(data.f_id, temp);
      if (a.index === -1) {
        temp.push(data);
      } else {
        temp[a.index].count = a.count;
      }
      let count = 0;
      for (let i = 0; i < temp.length; i++) {
        count += temp[i].count - 0;
      }
      this.cart_count = count;
      this.cart = temp;
      this.s.set('cart', temp);
      // this.translate.get("add").subscribe(add => {
      //   this.translate.get("success").subscribe(success => {
      // this.service.Toast(add + ' ' + data.name + " " + success);
      setTimeout(() => {
        this.s.get('cart').then((order) => {
          this.cart = order;
          this.changeDetector.detectChanges();
        });
      }, 10);
      //   });
      // });
    });
  }
  async presentModal(food2, index) {
    console.log(food2, index);
    
    let food = JSON.parse(JSON.stringify(food2));
    const modal = await this.modalController.create({
      component: ProductDetailPage
      ,
      cssClass: 'my-custom-modal-css1',
      componentProps: { data: food, index: index }
    });
    modal.onDidDismiss().then((data: any) => {
      setTimeout(() => {
        this.s.get('cart').then((order) => {
          this.cart = order;
          this.changeDetector.detectChanges();
        });
      }, 10);
    });
    await modal.present();
  }
  select_food(food2) {
    let food = JSON.parse(JSON.stringify(food2));
    if (food.status - 0 === 0) {
      // this.translate.get("oos_text").subscribe(oos_text => {
      this.api.Toast('สินค้าหมดแล้ว');
      // });
    } else {
      if (food.details.length == 0 && food.toppings.length == 0) {
        this.add_product(food);
      } else {
        this.presentModal(food, -1);
      }
    }
  }
  show_cart() {
    this.modalCtrl.dismiss({ close: true });
  }
  ngOnInit() {
    this.s.get('cart').then((temp) => {
      if (!temp) { temp = []; }
      let count = 0;
      this.cart = temp;
      for (let i = 0; i < temp.length; i++) {
        count += temp[i].count - 0;
      }
      this.cart_count = count;
    });
    this.api.get('load_product/' + this.fg_id).subscribe((res: any) => {
      console.log(res);
      this.items = res.data;
    })
  }
  back() {
    this.modalCtrl.dismiss({ close: false });
  }
}
