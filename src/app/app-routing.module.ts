import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },  {
    path: 'restaurant-menu',
    loadChildren: () => import('./restaurant-menu/restaurant-menu.module').then( m => m.RestaurantMenuPageModule)
  },
  {
    path: 'order-in-cart',
    loadChildren: () => import('./order-in-cart/order-in-cart.module').then( m => m.OrderInCartPageModule)
  },
  {
    path: 'not-found-restaurant',
    loadChildren: () => import('./not-found-restaurant/not-found-restaurant.module').then( m => m.NotFoundRestaurantPageModule)
  }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
