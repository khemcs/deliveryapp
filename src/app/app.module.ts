import { Register2Page } from './register2/register2.page';
import { Login2Page } from './login2/login2.page';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OptPage } from './opt/opt.page';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import firebaseCon from './firebase'
import { environment } from '../environments/environment';
import { GroupPage } from './group/group.page';
import { LocationPage } from './location/location.page';
import { ProductDetailPage } from './product-detail/product-detail.page';
import { ProductListPage } from './product-list/product-list.page';

export const firebaseConfig = {
  apiKey: "AIzaSyBykAj72z0Z8xrF65xH3NPy6m7BSf70v14",
  authDomain: "panda-restaurant.firebaseapp.com",
  databaseURL: "https://panda-restaurant.firebaseio.com",
  projectId: "panda-restaurant",
  storageBucket: "panda-restaurant.appspot.com",
  messagingSenderId: "1026422127449"
};

@NgModule({

  declarations: [AppComponent, Login2Page, 
    // Register2Page, OptPage, 
    GroupPage, ProductDetailPage, ProductListPage, LocationPage],
  entryComponents: [Login2Page, 
    // Register2Page, 
    // OptPage, 
    GroupPage, ProductDetailPage, ProductListPage, LocationPage],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, SweetAlert2Module.forRoot(),
    // GeolocationPluginWeb,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    // AngularFireModule.initializeApp(firebaseCon),
    AngularFireModule.initializeApp(firebaseConfig),

    AngularFireDatabaseModule,
    IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen, Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
