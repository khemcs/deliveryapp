import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found-restaurant',
  templateUrl: './not-found-restaurant.page.html',
  styleUrls: ['./not-found-restaurant.page.scss'],
})
export class NotFoundRestaurantPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
