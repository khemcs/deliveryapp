import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundRestaurantPage } from './not-found-restaurant.page';

const routes: Routes = [
  {
    path: '',
    component: NotFoundRestaurantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotFoundRestaurantPageRoutingModule {}
