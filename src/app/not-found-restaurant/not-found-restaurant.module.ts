import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotFoundRestaurantPageRoutingModule } from './not-found-restaurant-routing.module';

import { NotFoundRestaurantPage } from './not-found-restaurant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotFoundRestaurantPageRoutingModule
  ],
  declarations: [NotFoundRestaurantPage]
})
export class NotFoundRestaurantPageModule {}
