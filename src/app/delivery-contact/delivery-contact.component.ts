import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ApiServiceService } from '../api-service.service';
declare var google;

@Component({
  selector: 'app-delivery-contact',
  templateUrl: './delivery-contact.component.html',
  styleUrls: ['./delivery-contact.component.scss'],
})
export class DeliveryContactComponent implements OnInit {
  @Input() delivery: any = { member: { mm_name: '' } };
  @Input() data: any;
  constructor(private geolocation: Geolocation,
    public router: Router,
    public api: ApiServiceService,
  ) {
    
  }
  mm_name = '';
  rider_send = false;
  res = "";
  // data: any = {
  //   logo_url: '',
  //   name: '',
  //   address: '',
  //   id_res_auto: '',
  //   cover_url: ''
  // };
  // orddeliveryer = { status: '4', items: [], q: '1' };
  ngOnInit() {
    console.log(this.delivery);
    this.delivery = {
      member: {
        mm_name: ''
      },
      address: {
        address: '',
      }
    }

  }
  login(){
    
  }

  // check_delivery() {
  //   if (this.data.id_res_auto != '') {
  //     this.api.get("check_delivery/" + this.data.id_res_auto + '/' + this.user.mm_id).subscribe((res: any) => {
  //       console.log(res);
  //       if (res.flag) {
  //         this.show = 2;
  //         this.delivery = res.data;
  //         console.log(this.delivery);
  //       } else {
  //         this.show = 1;
  //       }
  //     });
  //   } else {
  //     setTimeout(() => {
  //       this.check_delivery();
  //     }, 500);
  //   }
  // }


}
