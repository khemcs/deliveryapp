import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigAddressComponent } from './config-address/config-address.component';
import { ConfigPasswordComponent } from './config-password/config-password.component';
import { ConfigProfileComponent } from './config-profile/config-profile.component';
import { HistoryBookingComponent } from './history-booking/history-booking.component';
import { HistoryDeliveryComponent } from './history-delivery/history-delivery.component';
import { Tab2Page } from './tab2.page';

const routes: Routes = [
  {
    path: '',
    component: Tab2Page,
  },
  {
    path: 'history-delivery',
    component: HistoryDeliveryComponent,
  },
  {
    path: 'history-booking',
    component: HistoryBookingComponent,
  },
  {
    path: 'config-profile',
    component: ConfigProfileComponent,
  },
  {
    path: 'config-address',
    component: ConfigAddressComponent,
  },
  {
    path: 'config-password',
    component: ConfigPasswordComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}
