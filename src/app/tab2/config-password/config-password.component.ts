import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-config-password',
  templateUrl: './config-password.component.html',
  styleUrls: ['./config-password.component.scss'],
})
export class ConfigPasswordComponent implements OnInit {
  public formPass: FormGroup
  constructor(
    private api: ApiServiceService,
    private strore: Storage,
    private alert: AlertController
  ) {
    this.formPass = new FormGroup({
      old_pass: new FormControl('', Validators.compose([Validators.required,Validators.pattern('[a-zA-Z0-9]*')])),
      new_pass: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12),Validators.pattern('[a-zA-Z0-9]*')])),
      confirm_pass: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12),Validators.pattern('[a-zA-Z0-9]*')])),
      mm_id: new FormControl(0)
    })
  }

  ngOnInit() {
    this.loadUser()
  }

  public data: any = {}
  loadUser() {
    this.strore.get('user').then((res: any) => {
      if (res) {
        this.formPass.patchValue({ mm_id: res.mm_id })
      }
    })
  }

  async update() {
    console.log(this.formPass);
    var new_pass = this.formPass.value.new_pass
    var confirm_pass = this.formPass.value.confirm_pass
    if (new_pass == confirm_pass) {
      await this.api.post('updatePassword', this.formPass.value).subscribe((obj_error: any) => {
        this.presentAlert(obj_error)
      })
    } else {
      var error = { header: 'ผิดผลาด!', subHeader: 'รหัสผ่านใหม่ไม่ตรงกัน.', error: true }
      this.presentAlert(error)
    }
  }

  async presentAlert(data) {
    const alert = await this.alert.create({
      mode: 'ios',
      header: data.header,
      subHeader: data.subHeader,
      buttons: [
        {
          text: 'ตกลง',
          handler: (e = data) => {
            if (!e.error) {
              window.location.reload()
            }
          }
        }
      ]
    });
    await alert.present();
  }
}
