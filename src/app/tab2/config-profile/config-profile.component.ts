import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-config-profile',
  templateUrl: './config-profile.component.html',
  styleUrls: ['./config-profile.component.scss'],
})
export class ConfigProfileComponent implements OnInit {
  public formProfile: FormGroup
  constructor(
    private api: ApiServiceService,
    private store: Storage,
    private alert: AlertController
  ) {
    this.formProfile = new FormGroup({
      mm_name: new FormControl('', Validators.compose([Validators.required])),
      // phone_id: new FormControl('', Validators.compose([Validators.required,Validators.minLength(9),Validators.maxLength(10),Validators.pattern('[0-9]*')])),
      pic_url: new FormControl(''),
      mm_id: new FormControl(0)
    })
  }

  ngOnInit() {
    this.loadUser()
  }

  public user: any = {}
  loadUser() {
    this.store.get('user').then((res: any) => {
      if (res) {
        this.user = res
        if (res.pic_url != '') {
          this.src = res.pic_url
        }
        this.formProfile.patchValue({ mm_id: this.user.mm_id, mm_name: this.user.mm_name })
      }
    })
  }

  public src: any = 'https://deltafood.co/new_logo.png'
  fileChanged(event) {
    const files = event.target
    if (files.files && files.files[0]) {
      var reader = new FileReader();
      reader.onload = (e) => {
        this.src = e.target.result
        this.formProfile.patchValue({ pic_url: this.src })
      }
      reader.readAsDataURL(files.files[0]);
    } else {
      this.src = 'https://deltafood.co/new_logo.png'
      this.formProfile.patchValue({ pic_url: '' })
    }
  }

  update() {
    this.api.post('updateProfile', this.formProfile.value).subscribe((error: any) => {
      this.presentAlert(error)
    })
  }

  async presentAlert(error) {
    const alert = await this.alert.create({
      mode: 'ios',
      header: error.header,
      subHeader: error.subHeader,
      buttons: [
        {
          text: 'ตกลง',
          handler: (e = error, mm_id = this.user.mm_id) => {
            if (!e.error) {
              this.api.post('getUser', { user_id: mm_id }).subscribe(async (res: any) => {
                if (res) {
                  await this.store.set('user', res)
                  await window.location.reload()
                } else {
                  window.location.reload()
                }
              })
            }
          }
        }
      ]
    });

    await alert.present();
  }

}
