import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/api-service.service';
import { ConfigAddressModalComponent } from './config-address-modal/config-address-modal.component';

@Component({
  selector: 'app-config-address',
  templateUrl: './config-address.component.html',
  styleUrls: ['./config-address.component.scss'],
})
export class ConfigAddressComponent implements OnInit {

  constructor(private api: ApiServiceService, private modalCtr: ModalController, private store: Storage) { }

  ngOnInit() {
    this.loadUser()
  }

  public user: any = []
  loadUser() {
    this.store.get('user').then((res: any) => {
      if (res) {
        this.user = res
        this.loadAddressConfig(this.user.mm_id)
      }
    })
  }

  public address: any = []
  loadAddressConfig(user_id) {
    this.api.get('loadAddressConfig/' + user_id).subscribe((res) => {
      if (res) {
        this.address = res
      }
    })
  }

  async openAddress(data) {
    const modal = await this.modalCtr.create({
      component: ConfigAddressModalComponent,
      componentProps: { local_data: data }
    })

    modal.onWillDismiss().then((event: any) => {
      if(event.role == 'onDissmiss' && !event.data.error){
        this.loadAddressConfig(this.user.mm_id)
      }
    })

    return modal.present()
  }
}
