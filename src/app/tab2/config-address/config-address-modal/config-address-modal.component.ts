import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/api-service.service';
declare var google;

@Component({
  selector: 'app-config-address-modal',
  templateUrl: './config-address-modal.component.html',
  styleUrls: ['./config-address-modal.component.scss'],
})
export class ConfigAddressModalComponent implements OnInit {

  public formAddress: FormGroup
  constructor(private geolocation: Geolocation, public store: Storage, private modalCtr: ModalController, private api: ApiServiceService, private alert: AlertController) {
    this.formAddress = new FormGroup({
      local_name: new FormControl('', Validators.compose([Validators.required])),
      local_id: new FormControl(0),
      local_lat: new FormControl(0),
      local_long: new FormControl(0),
    })
  }


  @Input() local_data
  ngOnInit() {
    setTimeout(() => {
      this.loadMap()
      console.log(this.local_data);
    }, 1000);
  }

  @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;
  public latitude: any;
  public longitude: any;
  async loadMap() {
    this.geolocation.getCurrentPosition().then(async (resp) => {
      if (!this.local_data || (this.local_data.local_lat == undefined || this.local_data.local_long == undefined)) {
        this.latitude = await resp.coords.latitude;
        this.longitude = await resp.coords.longitude;
        this.formAddress.patchValue({
          local_lat: this.latitude,
          local_long: this.longitude
        })
      } else {
        this.latitude = await parseFloat(this.local_data.local_lat);
        this.longitude = await parseFloat(this.local_data.local_long);
        this.formAddress.patchValue({
          local_id: this.local_data.local_id,
          local_name: this.local_data.local_name,
          local_lat: this.latitude,
          local_long: this.longitude
        })
      }

      const pos = await {
        lat: this.latitude,
        lng: this.longitude
      };
      const map = await new google.maps.Map(this.mapNativeElement.nativeElement, {
        center: pos,
        zoom: 17
      });
      await map.setCenter(pos);
      const icon = await {
        url: 'assets/icon/icon_map/u.png', // image url
        scaledSize: new google.maps.Size(50, 50), // scaled size
      };
      const marker = await new google.maps.Marker({
        position: pos,
        map: map,
        title: 'Hello World!',
        icon: icon
      });
      await map.addListener('idle', (e) => {
        marker.setPosition(map.getCenter());
        this.latitude = marker.getPosition().lat();
        this.longitude = marker.getPosition().lng();
        this.formAddress.patchValue({
          local_lat: this.latitude,
          local_long: this.longitude
        })
      });

    }).catch((error) => {
      console.log('Error getting location', error);
    })
  }

  update() {
    this.api.post('updateLocationConfig', this.formAddress.value).subscribe(async (error: any) => {
      if (!error) {
        const alert = await this.alert.create({
          mode : 'ios',
          header: 'สำเร็จ!',
          subHeader: 'อัพเดทสถานที่จัดส่งเรียบร้อย.',
          buttons: [{
            text: 'ตกลง',
            handler: () => {
              this.modalCtr.dismiss({ error: false }, 'onDissmiss')
            }
          }]
        });

        await alert.present();
      }

    })
  }

  async delete(local_id) {
    const alert = await this.alert.create({
      mode: 'ios',
      header: 'ลบสถานที่จัดส่ง!',
      subHeader: 'กด ตกลง เพื่อลบสถานที่จัดส่ง.',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel'
        },
        {
          text: 'ตกลง',
          handler: () => {
            this.api.post('deleteLocationConfig', { local_id: local_id }).subscribe(async (error: any) => {
              if (!error) {
                const alert = await this.alert.create({
                  mode: 'ios',
                  header: 'สำเร็จ!',
                  subHeader: 'ลบสถานที่จัดส่งเรียบร้อย.',
                  buttons: [{
                    text: 'ตกลง',
                    handler: () => {
                      this.modalCtr.dismiss({ error: false }, 'onDissmiss')
                    }
                  }]
                });

                await alert.present();
              }
            })
          }
        }
      ]
    });

    await alert.present();
  }

  close() {
    this.modalCtr.dismiss()
  }

}
