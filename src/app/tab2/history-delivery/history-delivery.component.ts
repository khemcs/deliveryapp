import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/api-service.service';
import { HistoryDeliveryModalComponent } from './history-delivery-modal/history-delivery-modal.component';
declare var liff: any;
declare var require: any
var moment = require('moment'); // require

@Component({
  selector: 'app-history-delivery',
  templateUrl: './history-delivery.component.html',
  styleUrls: ['./history-delivery.component.scss'],
})
export class HistoryDeliveryComponent implements OnInit {
  public customDayShortNames = ['s\u00f8n', 'man', 'tir', 'ons', 'tor', 'fre', 'l\u00f8r'];
  public data: any = { id_res_auto: '' }
  constructor(public api: ApiServiceService, public store: Storage, public modalCtr: ModalController) {

    this.store.get('restaurant').then(async (res: any) => {
      if (res) {
        this.data = await res
        await this.liff_init()
      }
    })
  }

  ngOnInit() {
    this.HistoryDelivery(this.dS, this.dE, this.page)
  }


  HistoryDelivery(dateS, dateE, page) {
    if (this.user.mm_id != '' && this.data.id_res_auto != '') {
      this.getHistoryDelivery(dateS, dateE, page)
    } else {
      setTimeout(() => {
        this.HistoryDelivery(dateS, dateE, page)
      }, 500);
    }
  }

  public localhost = window.location.href.indexOf("localhost") != -1;
  public user: any = { mm_id: '' }
  async liff_init() {
    await liff.init({ liffId: this.data.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f",
        'id_res_auto': this.data.id_res_auto
      };
      this.api.post("update_profile", data).subscribe((res) => {
        console.log(res);
        this.user = res;
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {
      console.log("liff.isInClient() || liff.isLoggedIn()");
      this.getUserProfile();
    } else {
      // liff.login({ redirectUri: window.location.href })
    }
  }

  async getUserProfile() {
    const profile = await liff.getProfile();
    profile.id_res_auto = this.data.id_res_auto;
    this.api.post("update_profile", profile).subscribe((res) => {
      this.user = res;

    });
  }

  public history_delivery: any = []
  public countList: number = 0
  getHistoryDelivery(dateS, dateE, page) {
    var data = {
      // member_id: this.user.mm_id,
      member_id: '68',
      id_res_auto: this.data.id_res_auto,
      dateStart: dateS,
      dateEnd: dateE,
      page: page
    }
    this.api.post('getHistoryDelivery', data).subscribe((res: any) => {
      res.data.forEach(el => {
        this.history_delivery.push(el)
      });
      this.countList = res.count
    })
  }

  async seeMore(hd) {
    const modal = await this.modalCtr.create({
      component: HistoryDeliveryModalComponent,
      componentProps: { delivery: hd }
    })
    await modal.present()
    return await modal;
  }

  public dS: any = moment().format('YYYY-MM-DD')
  public dE: any = moment().add(1, 'day').format('YYYY-MM-DD')
  async onBydate() {
    let start = await moment(this.dS).format('YYYY-MM-DD')
    let end = await moment(this.dE).format('YYYY-MM-DD')
    this.page = 1
    var data = {
      // member_id: this.user.mm_id,
      member_id: '68',
      id_res_auto: this.data.id_res_auto,
      dateStart: start,
      dateEnd: end,
      page: this.page
    }
    this.api.post('getHistoryDelivery', data).subscribe((res: any) => {
      this.history_delivery = res.data
      this.countList = res.count
    })
  }

  public page: number = 1
  async loadData(event) {
    await setTimeout(() => {
      console.log('Done');
      event.target.complete();
      if (this.history_delivery.length > this.countList) {
        event.target.disabled = true;
      } else {
        this.page++
        let start = moment(this.dS).format('YYYY-MM-DD')
        let end = moment(this.dE).format('YYYY-MM-DD')
        this.HistoryDelivery(start, end, this.page)
      }
    }, 500);
  }
}
