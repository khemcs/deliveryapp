import { ModalController } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-history-delivery-modal',
  templateUrl: './history-delivery-modal.component.html',
  styleUrls: ['./history-delivery-modal.component.scss'],
})
export class HistoryDeliveryModalComponent implements OnInit {

  @Input() delivery: any = { member: { mm_name: '' } };
  constructor(public api: ApiServiceService, public modal: ModalController) { }


  ngOnInit() {
    setTimeout(() => {
      this.loadOrderDetail()
    }, 500);
  }

  loadOrderDetail() {
    this.api.get('check_delivery_history/' + this.delivery.id_res_auto + '/' + this.delivery.order_id).subscribe((res: any) => {
      if (res.flag) {
        this.delivery = res.data
      }
    })
  }

  get_html(c): String {
    let out: String = '';
    console.log(c.details);
    for (let entry of c.details) {
      // let price = 0;
      let temp = '';
      let flag = true;
      for (let sub of entry.sub) {
        if (sub.selected) {
          if (flag) {
            flag = false;
            temp += sub.name[0].title;
          } else {
            temp += ", " + sub.name[0].title;
          }
        }
      }
      if (temp != '') {
        out += entry.name[0].title + " : ";
        out += temp;
        out += "<br>";
      }

    }

    for (let entry of c.toppings) {
      if (entry.count - 0 > 0) {
        out += "  [" + entry.count + "] " + entry.title + " (";
        if (entry.price > 0) {
          out += "+";
        }
        out += (entry.price * entry.count) + ")";
        out += "<br>";
      }
    }
    if (c.comments.trim() != '') {
      out += "Comment : " + c.comments;
    }

    return out;
  }

  get_price(c): number {
    let out = 0;
    if (c.is_buffet - 0 == 1) {
      out = 0;
    } else {
      out = parseFloat(c.price);
    }

    for (let entry of c.details) {
      for (let sub of entry.sub) {
        if (sub.selected) {
          out += parseFloat(sub.price);
        }
      }
    }
    for (let entry of c.toppings) {
      if (Number(entry.count) > 0) {
        out += parseFloat(entry.price) * Number(entry.count);
      }
    }
    return out * Number(c.count);
  }

  cooking() {
    let flag = false;
    this.delivery.items.forEach(element => {
      if (element.status != '0') {
        flag = true;
      }
    });
    return flag;
  }
  finish() {
    let flag = true;
    this.delivery.items.forEach(element => {
      if (element.status == '0' || element.status == '1') {
        flag = false;
      }
    });
    return flag;
  }

  close() {
    this.modal.dismiss();
  }

}
