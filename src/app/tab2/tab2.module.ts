import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { Tab2PageRoutingModule } from './tab2-routing.module';
import { ConfigProfileComponent } from './config-profile/config-profile.component';
import { ConfigAddressComponent } from './config-address/config-address.component';
import { ConfigPasswordComponent } from './config-password/config-password.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ConfigAddressModalComponent } from './config-address/config-address-modal/config-address-modal.component';
import { HistoryDeliveryComponent } from './history-delivery/history-delivery.component';
import { HistoryBookingComponent } from './history-booking/history-booking.component';
import { HistoryBookingModalComponent } from './history-booking/history-booking-modal/history-booking-modal.component';
import { HistoryDeliveryModalComponent } from './history-delivery/history-delivery-modal/history-delivery-modal.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExploreContainerComponentModule,
    Tab2PageRoutingModule
  ],
  declarations: [
    Tab2Page,
    HistoryDeliveryComponent,
    HistoryDeliveryModalComponent,
    HistoryBookingComponent,
    HistoryBookingModalComponent,
    ConfigProfileComponent,
    ConfigAddressComponent,
    ConfigPasswordComponent,
    ConfigAddressModalComponent
  ],
  entryComponents : [ConfigAddressModalComponent,HistoryDeliveryModalComponent,HistoryBookingModalComponent],
  providers : [
    Geolocation
  ]
  // schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Tab2PageModule { }
