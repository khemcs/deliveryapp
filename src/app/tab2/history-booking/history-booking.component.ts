import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/api-service.service';
import { HistoryBookingModalComponent } from './history-booking-modal/history-booking-modal.component';
declare var liff: any;

@Component({
  selector: 'app-history-booking',
  templateUrl: './history-booking.component.html',
  styleUrls: ['./history-booking.component.scss'],
})
export class HistoryBookingComponent implements OnInit {

  constructor(public api: ApiServiceService, public store: Storage, public modalCtr: ModalController) { }

  public data: any = { id_res_auto: '' }
  async ngOnInit() {
    this.store.get('restaurant').then(async (res: any) => {
      this.data = await res
      await this.liff_init()
    })
    this.awaitHistoryBooking()
  }

  awaitHistoryBooking() {
    if (this.user.mm_id != '' && this.data.id_res_auto != '') {
      this.getHistoryBooking()
    } else {
      setTimeout(() => {
        this.awaitHistoryBooking()
      }, 500);
    }
  }

  public localhost = window.location.href.indexOf("localhost") != -1;
  public user: any = { mm_id: '' }
  async liff_init() {
    await liff.init({ liffId: this.data.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f",
        'id_res_auto': this.data.id_res_auto
      };
      this.api.post("update_profile", data).subscribe((res) => {
        console.log(res);
        this.user = res;
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {
      console.log("liff.isInClient() || liff.isLoggedIn()");
      this.getUserProfile();
    } else {
      // liff.login({ redirectUri: window.location.href })
    }
  }

  async getUserProfile() {
    const profile = await liff.getProfile();
    profile.id_res_auto = this.data.id_res_auto;
    this.api.post("update_profile", profile).subscribe((res) => {
      this.user = res;
    });
  }

  public history_bk: any = []
  getHistoryBooking() {
    var data = {
      mm_id: this.user.mm_id,
      id_res_auto: this.data.id_res_auto
    }
    console.log(this.user);

    this.api.post('getHistoryBooking', data).subscribe((res: any) => {
      this.history_bk = res
      console.log(res);

    })
  }

  async moreDetail(bk) {
    const modal = await this.modalCtr.create({
      component: HistoryBookingModalComponent,
      componentProps: { data_bk: bk }
    })
    modal.onDidDismiss().then((res) => {
      console.log(res);

    })
    await modal.present()
    return await modal
  }
}
