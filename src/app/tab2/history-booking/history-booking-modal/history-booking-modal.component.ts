import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-history-booking-modal',
  templateUrl: './history-booking-modal.component.html',
  styleUrls: ['./history-booking-modal.component.scss'],
})
export class HistoryBookingModalComponent implements OnInit {
  @Input() data_bk: any = {}
  constructor(public modalCtr: ModalController,public api : ApiServiceService) { }

  ngOnInit() {
    setTimeout(() => {
      this.check_user_booking()
    }, 500);
   }

   public data : any = {}
  check_user_booking() {
    if (this.data_bk.mm_id != '') {
      this.api.get("eheck_booking/" + this.data_bk.id_res_auto + "/" + this.data_bk.mm_id).subscribe((res: any) => {
        console.log(res);
        if (res.flag) {
          this.data = res.data;
        } 
      });
    } else {
      setTimeout(() => {
        this.check_user_booking();
      }, 500);

    }
  }

  get_html(c): String {
    let out: String = '';
    // console.log(c.details);
    for (let entry of c.details) {
      // let price = 0;
      let temp = '';
      let flag = true;
      for (let sub of entry.sub) {
        if (sub.selected) {
          if (flag) {
            flag = false;
            temp += sub.name[0].title;
          } else {
            temp += ", " + sub.name[0].title;
          }
        }
      }
      if (temp != '') {
        out += entry.name[0].title + " : ";
        out += temp;
        out += "<br>";
      }

    }

    for (let entry of c.toppings) {
      if (entry.count - 0 > 0) {
        out += "  [" + entry.count + "] " + entry.title + " (";
        if (entry.price > 0) {
          out += "+";
        }
        out += (entry.price * entry.count) + ")";
        out += "<br>";
      }
    }
    if (c.comments.trim() != '') {
      out += "Comment : " + c.comments;
    }

    return out;
  }
  get_price(c): number {
    let out = 0;
    if (c.is_buffet - 0 == 1) {
      out = 0;
    } else {
      out = parseFloat(c.price);
    }

    for (let entry of c.details) {
      for (let sub of entry.sub) {
        if (sub.selected) {
          out += parseFloat(sub.price);
        }
      }
    }
    for (let entry of c.toppings) {
      if (Number(entry.count) > 0) {
        out += parseFloat(entry.price) * Number(entry.count);
      }
    }
    return out * Number(c.count);
  }

  close() {
    this.modalCtr.dismiss()
  }
}
