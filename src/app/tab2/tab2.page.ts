import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular'
import { ApiServiceService } from '../api-service.service';
declare var liff: any;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  public localhost = window.location.href.indexOf("localhost") != -1;
  constructor(public storage: Storage,
    public api: ApiServiceService,
    private route: Router,
    private alertController: AlertController) {

  }

  public restaurant: any = {};
  public user: any = { mm_id: '' };
  public pic_default = 'https://cdn-icons-png.flaticon.com/512/1077/1077012.png'
  async liff_init() {
    await liff.init({ liffId: this.restaurant.line_settings.liff_delivery });
    console.log(this.localhost);

    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f",
        'id_res_auto': this.restaurant.id_res_auto
      };
      this.api.post("update_profile", data).subscribe((res) => {
        console.log(res);
        this.user = res;
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {

      this.getUserProfile();
    } else {
      liff.login({ redirectUri: window.location.href })
    }
  }
  async getUserProfile() {
    const profile = await liff.getProfile();
    profile.id_res_auto = this.restaurant.id_res_auto;
    this.api.post("update_profile", profile).subscribe((res) => {
      this.user = res;
    });
  }

  ngOnInit() {
    this.load_data();
  }

  load_data() {
    this.storage.get('restaurant').then((val: any) => {
      console.log(val);
      this.restaurant = val
      this.liff_init();
    })
    // this.api.get("get_res/" + this.res).subscribe((res: any) => {
    //   this.storage.set('restaurant', res.data);
    //   this.restaurant = res.data;
    //   this.liff_init();
    // })
  }

  // async persentAlert(title: string, message: string){
  //   const alert = await this.alertController.create({
  //     header: title,
  //     message,
  //     buttons:['OK']
  //   })
  //   await alert.present()

  //     }

  logout() {
    this.storage.set("login", null);
    this.storage.set("user", null);

    //     this.persentAlert('Logout Success', ' ออกจากระบบสำเร็จ!')
    // this.route.navigate(['/home'], { replaceUrl: true });
  }


}
