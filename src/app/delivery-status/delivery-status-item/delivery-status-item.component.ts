import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-delivery-status-item',
  templateUrl: './delivery-status-item.component.html',
  styleUrls: ['./delivery-status-item.component.scss'],
})
export class DeliveryStatusItemComponent implements OnInit {
  @Input() detail: any = {}
  constructor(private api: ApiServiceService, private store: Storage, private modalCtr: ModalController) { }

  ngOnInit() {
    setTimeout(() => {
      console.log(this.detail);
      this.totalPrice()
    }, 500);
  }

  public sum: number = 0
  public total : any = 0
  totalPrice() {
    // console.log(this.total);
    this.detail.option.forEach(el => {
      if (el.count != null || el.price != null) {
        this.sum += (el.count * parseInt(el.price)) + parseInt(this.detail.price)
      }
    });

    if(this.sum == 0){
      this.sum = this.detail.price * this.detail.count
      this.total = this.sum
    }else{
      this.total = this.sum * this.detail.count
    }
  }

  close() {
    this.modalCtr.dismiss()
  }

}
