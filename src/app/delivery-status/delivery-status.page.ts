import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from '../api-service.service';
import { DeliveryStatusItemComponent } from './delivery-status-item/delivery-status-item.component';
import * as moment from 'moment';
declare var google;

@Component({
  selector: 'app-delivery-status',
  templateUrl: './delivery-status.page.html',
  styleUrls: ['./delivery-status.page.scss'],
})
export class DeliveryStatusPage implements OnInit {
  @Input() order: any = {}
  public res: any = {}
  public time: string = ''
  constructor(private routerActiva: ActivatedRoute, private router: Router, private api: ApiServiceService, private store: Storage, private geolocation: Geolocation, private modalCtr: ModalController) {
    this.store.get('restaurant').then((res_data: any) => {
      if (res_data) {
        this.res = res_data
      }
    })
  }

  public realtime: boolean = true
  public rider: any = {}
  async ionViewWillEnter() {
    this.realtime = true
    if (this.realtime) {
      await this.getRouteRider()
      await this.loadOrderData()
    }
  }

  ngOnInit() {
    console.log(this.order);
    setTimeout(() => {
      this.loadMap()
      this.deliveryTotal()
    }, 500);
  }

  settime() {
    let duration = this.order.order_data.address.duration / 60
    let end_send = moment(this.order.order_data.up_date).add(duration, 'minutes').format('HH:mm')
    this.time = moment(this.order.order_data.up_date).format('HH:mm') + " - " + end_send + " น."
  }

  loadOrderData() {
    this.settime()
  }

  async getRouteRider() {
    let id_rider = await 20
    await this.api.fb_get(this.res.id_res_auto + '_' + this.res.code + '/rider/' + id_rider, async (res_local: any) => {
      if (res_local.val()) {
        console.log(res_local.val().lat);
        this.rider = await {
          lat: res_local.val().lat,
          long: res_local.val().lng
        }
        await this.calcRoute(this.directionsService, this.directionsRenderer)
      } else if(!res_local.val() && res_local.val() != null){
        this.getRouteRider()
      }

    })
  }

  @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;
  loadMap() {
    this.geolocation.getCurrentPosition().then(async (resp) => {
      var pos = await {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
      if (this.order.restaurant.location) {
        let local_tmp = this.order.restaurant.location.split(',')
        let local = {
          lat: parseFloat(local_tmp[0]),
          lng: parseFloat(local_tmp[1]),
        }
        pos = local
      }
      const map = await new google.maps.Map(this.mapNativeElement.nativeElement, {
        center: pos,
        zoom: 15,
      });
      this.directionsRenderer.setMap(map);
      // const icon = await {
      //   url: 'assets/icon/icon_map/u.png', // image url
      //   scaledSize: new google.maps.Size(50, 50), // scaled size
      // };
      // const marker = await new google.maps.Marker({
      //   position: pos,
      //   map: map,
      //   title: 'Hello World!',
      //   icon: icon
      // });
      // await map.addListener('idle', (e) => {
      //   marker.setPosition(map.getCenter());
      //   // this.latitude = marker.getPosition().lat();
      //   // this.longitude = marker.getPosition().lng();
      // });

    }).catch((error) => {
      console.log('Error getting location', error);
    })
  }

  public directionsService = new google.maps.DirectionsService();
  public directionsRenderer = new google.maps.DirectionsRenderer();
  calcRoute(directionsService, directionsRenderer) {
    var haight = new google.maps.LatLng(this.rider.lat, this.rider.long);
    var oceanBeach = new google.maps.LatLng(16.4475804, 102.8233044);
    var request = {
      origin: haight,
      destination: oceanBeach,
      travelMode: google.maps.TravelMode['DRIVING'],
    };
    directionsService.route(request, (response, status) => {
      if (status == 'OK') {
        directionsRenderer.setOptions({ preserveViewport: true });
        directionsRenderer.setDirections(response);
      }
    });
  }

  totalAll : any = 0
  deliveryTotal() {
    this.order.qryOrderDetail.forEach((el) => {
      let sum = 0
      let count = 1
      el.toppings_price.forEach(el_tpsp => {
        sum += parseInt(el.total_price) + (parseInt(el_tpsp.price) * parseInt(el_tpsp.count))
        count = parseInt(el_tpsp.count)
      });
      if (count > 1) {
        sum = sum * count
      } else {
        sum = parseInt(el.total_price)
      }
      this.totalAll += sum
    });
  }

  public total: any = 0
  priceItemTotal(od) {
    let total = 0
    od.toppings_price.forEach(el => {
      total += parseInt(el.price) * parseInt(el.count)
    });
    total += parseInt(od.price)
    return total
  }

  deliveryItemDetail(order_detail) {
    let topping: any = {}
    this.api.post('deliveryItemDetail', { od_id: order_detail.od_id, details: order_detail.details, toppings: order_detail.toppings }).subscribe(async (res: any) => {
      topping = res
      order_detail.option = topping
      const modal = await this.modalCtr.create({
        component: DeliveryStatusItemComponent,
        componentProps: { detail: order_detail }
      })
      return await modal.present()
    })

  }

  back() {
    this.modalCtr.dismiss()
  }

  async doRefresh(event) {
    await this.getRouteRider()
    await this.calcRoute(this.directionsService, this.directionsRenderer)
    await setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  ionViewWillLeave() {
    this.realtime = false
  }
}
