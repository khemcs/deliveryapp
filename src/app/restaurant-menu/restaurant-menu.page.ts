import { ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';
import { Component, OnInit } from '@angular/core';
import { ProductDetailPage } from '../product-detail/product-detail.page';

@Component({
  selector: 'app-restaurant-menu',
  templateUrl: './restaurant-menu.page.html',
  styleUrls: ['./restaurant-menu.page.scss'],
})
export class RestaurantMenuPage implements OnInit {
  slideOtp: any = {
    slidesPerView: 3,
    autoplay: true,
  }
  back_btn = ''
  constructor(
    public api: ApiServiceService,
    public route: Router,
    public router: ActivatedRoute,
    public modalCtr: ModalController
  ) {
    this.api.liff_init()
    window.addEventListener("scroll", (e) => { })
  }

  checkTypeCart() {
    let type_cart: string = ''
    type_cart = this.api.getStore('cart_restaurant_menu') ? this.api.getStore('cart_restaurant_menu') : 'restaurant-menu'
    return type_cart
  }


  _orderInCart: number = 0
  ngOnInit() {
    this.setData()
  }

  ionViewDidEnter() {
    this.check_delivery()
  }

  res: any = []
  async setData() {
    setTimeout(async() => {
      this.res = await this.api.getStore('restaurant')
      await this.loadGroup(this.res.id_res_auto)
    }, 1000);
  }

  _group: any = []
  loadGroup(key_res) {
    this.api.get('load_group/' + key_res).subscribe((res: any) => {
      if (res.flag) {
        this._group = res.data
        this._group.forEach((el, index) => {
          el.isPick = false
          if (index == 0) {
            el.isPick = true
            this.pickTypeMenu(index, el.fg_id)
          }
        });
      }
    })
  }

  showTypeMenu: string = 'none'
  logScrolling(e) {
    let y = e.detail.currentY
    if (y > 125.400) {
      this.showTypeMenu = 'block'
    } else {
      this.showTypeMenu = 'none'
    }
  }

  async pickFood(food, i) {
    if (food.status == 2) {
      if (food.details.length < 1) {
        await food.count++
        let cart = await this.api.getStore('cart')
        let temp: any = []
        let _inside = await false
        if (cart) {
          await cart.forEach(el => {
            if (food.f_id == el.f_id) {
              temp.push(food)
              _inside = true
            } else {
              temp.push(el)
            }
          });
          cart = await temp
          if (!_inside) {
            await cart.push(food)
          }
          await this.api.setStore('cart', temp)
        } else {
          this.api.setStore('cart', [food])
        }
      } else {
        // let food = JSON.parse(JSON.stringify(food2));
        const modal = await this.modalCtr.create({
          component: ProductDetailPage
          ,
          cssClass: 'my-custom-modal-css1',
          componentProps: { data: food }
        });
        modal.onDidDismiss().then((res: any) => {
          if (res.role === 'response') {
            // food = res.data
            food.count = res.countPick
            setTimeout(() => {
            }, 1000);

          }
        });
        await modal.present();
      }
    }

  }
  p_by_w() {
    let food = this.api.getStore('cart')
    let out = false;
    if (food) {
      food.forEach(c => {
        if (c.p_by_w == '1') {
          out = true;
          return true;
        }
      });
    }

    return out;
  }

  checkAmount() {
    let food = this.api.getStore('cart')
    let amount = 0
    if (food) {
      food.forEach(el => {
        amount += el.count
      });
    }

    return amount
  }

  checkTotalPrice() {
    let food = this.api.getStore('cart')
    let total = 0
    if (food) {
      food.forEach(el => {
        for (let entry of el.details) {
          for (let sub of entry.sub) {
            if (sub.selected) {
              total += parseFloat(sub.price) * el.count;
            }
          }
        }
        for (let entry of el.toppings) {
          if (Number(entry.count) > 0) {
            total += parseFloat(entry.price) * Number(entry.count);
          }
        }
        if (el.p_by_w == '0') {
          total += el.price * el.count
        }
      });
    }

    return total
  }

  _menu: any = []
  _countByFood: any = []
  async pickTypeMenu(i, g_id) {
    await this._group.forEach((el, index) => {
      el.isPick = false
      if (i == index) {
        el.isPick = true
      }
    });

    await this.api.get('load_product/' + g_id).subscribe((res: any) => {
      let food = this.api.getStore('cart')
      if (res.flag) {
        this._menu = res.data
        this._menu.forEach((el, index) => {
          el.count = 0
          if (food) {
            food.forEach(el_f => {
              if (el_f.f_id == el.f_id) {
                el.count = el_f.count
              }
            });
          }
        });
      }
    })
  }

  activeCountFood(f_id, i) {
    let food = this.api.getStore('cart')
    this._menu.forEach((el) => {
      el.count = 0
      if (food) {
        food.forEach(el_f => {
          if (el_f.f_id == el.f_id) {
            el.count += el_f.count
          }
        });
      }
    })
    if (f_id == this._menu[i].f_id) {
      return this._menu[i].count
    }
  }

  orderIncart() {
    let type = this.checkTypeCart()
    if (type == 'restaurant-menu') {
      type = 'delivery'
    } else if (type == 'booking') {
      type = 'booking'
    } else if (type == 'receive') {
      type = 'receive'
    } else {
      type = 'delivery'
    }
    let key_restaurant = this.api.getStore('key_restaurant')
    this.route.navigate(['/' + key_restaurant + '/' + type])
  }

  public show = 0
  public fb_set = false;
  public delivery: any = []
  check_delivery() {
    if (this.res.id_res_auto != '') {
      let user: any = this.api.getStore('user')
      if (user.mm_id) {
        this.api.get("check_delivery/" + this.res.id_res_auto + '/' + user.mm_id).subscribe((res: any) => {
          if (res.flag) {
            this.show = 2
            if (!this.fb_set) {
              this.api.fb_get(this.res.id_res_auto + "_" + this.res.code + "/orders/" + res.data.order_id, (va) => {
                this.check_delivery();
              });
              setTimeout(() => {
                this.fb_set = true;
              }, 100);
            }
            this.delivery = res.data;
            if (this.delivery.status == '1') {
              setTimeout(() => {
                // this.onReview();


              }, 1000);
            }
            this.orderIncart()
          } else {
            this.show = 1
          }
        })
      } else {
        this.check_delivery();
      }
    } else {
      setTimeout(() => {
        this.check_delivery();
      }, 500);
    }
  }

}
