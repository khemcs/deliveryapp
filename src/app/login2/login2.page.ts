import { Register2Page } from './../register2/register2.page';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { ApiServiceService } from '../api-service.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login2',
  templateUrl: './login2.page.html',
  styleUrls: ['./login2.page.scss'],
})
export class Login2Page implements OnInit {


  public loginForm: FormGroup;

  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public storage: Storage,
    public alertController: AlertController,
    public api: ApiServiceService,
    public modalCtrl: ModalController) {
    this.loginForm = new FormGroup({
      username: new FormControl("",Validators.compose([
        Validators.required
      ])),
      password: new FormControl('',Validators.compose([Validators.required]))
    });

  }
  close() {
    this.modalCtrl.dismiss({ action: false });

  }
  onSignIn() {
    console.log('what is goin on');
    console.log(this.loginForm.value);
    // this.router.navigateByUrl('/referring-physician');
  }
  ngOnInit() {
  }
  async error() {
    const alert = await this.alertController.create({
      cssClass: 'alert_css',
      header: 'เบอร์โทรศัพท์หรือรหัสผ่านไม่ถูกต้อง',
      // subHeader: 'Subtitle',
      // message: 'This is an alert message.',
      buttons: ['OK']
    });
    alert.onDidDismiss().then((res) => {

    });
    await alert.present();
  }
  loginUser() {
    // console.log(this.loginForm);
    // console.log(this.loginForm.value);
    this.api.post("loginUser", this.loginForm.value).subscribe((res: any) => {
      console.log(res);
      if (res.flag == 'success') {
        this.storage.set('login', true);
        this.storage.set('user', res);
        this.modalCtrl.dismiss({
          action: true
        });
      } else {
        this.error();
      }

    })
  }
  reset_pass() {

  }
  async register() {

    const modal = await this.modalCtrl.create({
      component: Register2Page,
      componentProps: { data: {} },
      backdropDismiss: false,
      cssClass: 'regis_modal',
    });

    modal.onWillDismiss().then((event: any) => {

    })

    return await modal.present();
  }

}
