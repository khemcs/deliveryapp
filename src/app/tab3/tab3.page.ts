import { Storage } from '@ionic/storage';
import { ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ApiServiceService } from '../api-service.service';
import { GroupPage } from '../group/group.page';
import { LocationPage } from '../location/location.page';
import { async } from '@angular/core/testing';
import { Router } from '@angular/router';
import { SendReviewComponent } from '../tab1/review/send-review/send-review.component';
import { MyMapComponent } from '../my-map/my-map.component';
declare var google;
declare var liff: any;
declare var require: any
var moment = require('moment');

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})

export class Tab3Page {
  public cfState1 = false
  public cfState2 = false
  public cfState3 = true
  public localhost = window.location.href.indexOf("localhost") != -1;
  cart = [];
  res = '';
  show = 0;
  mm_name = '';
  rider_send = false;
  delivery: any = {
    member: {
      mm_name: ''
    },
    address: {
      address: '',
    }
  };
  public pay: any = { type: 0, id_pay_type: undefined }

  public locationMark: any = { end_address: '-', price: 0, text: '-' }
  public newLocation: string = ''
  public user: any = { mm_id: '' };
  public orderActive: any = { status: null }
  constructor(public modalCtrl: ModalController,
    public api: ApiServiceService,
    public s: Storage,
    private modalCtr: ModalController,
    public modalController: ModalController,
    public router: Router,
    public alert: AlertController,
    public deRef: ChangeDetectorRef
  ) {
    let checkStatus: any = this.api.getStore('status_system')
    if (checkStatus != null) {
      checkStatus.delivery = true
      this.api.setStore('status_system', checkStatus)
    } else {
      this.api.setStore('status_system', { delivery: true })
    }
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  c_name: string = ''
  c_phone: string = ''
  setContact() {
    this.c_name = this.api.getStore('c_name') ? this.api.getStore('c_name') : this.user.mm_name
    this.c_phone = this.api.getStore('c_phone') ? this.api.getStore('c_phone') : ''
  }

  check_page() {
    // if (this.cart.length == 0) {
    //   this.add_food();
    // } else if (this.locationMark.end_address == '-') {
    //   this.locatioin();
    // }

  }
  async getUserProfile() {
    const profile = await liff.getProfile();
    this.api.post("update_profile", profile).subscribe((res: any) => {
      this.user = res;
      this.api.fb_get(this.data.id_res_auto + "_" + this.data.code + "/data_pay", () => {
        this.check_delivery();
      });
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.check_page();
    }, 1000);

    // this.load_data();

    setTimeout(() => {
      if (this.show == 2) {
        this.initMap();
      }
    }, 1000);
    this.connect_fb();
  }

  _restaurant: any = []
  ionViewDidEnter() {
    this.load_data();
    this._isSuccess = 0
    this.loadCart()
    let my_pos = this.api.getStore('my_pos')
    this.directions(my_pos.lat, my_pos.lng, 0, null)
    this._restaurant = this.api.getStore('restaurant')

    // if (this._restaurant.bank.length > 0) {
    //   this._transferBank = this._restaurant.bank[0].id_pay_type
    // }

    setTimeout(() => {
      this.liff_init();
    }, 500);

    setTimeout(() => {
      this.setContact()
    }, 1500);
  }

  _isSuccess: number = 0
  ionViewWillLeave() {
    this._isSuccess = 0
  }

  p_by_w: boolean = false
  _cart: any = []
  async loadCart() {
    this.cart = await this.api.getStore('cart')
    if (this.cart) {
      this.cfState1 = true
      for (let c of this.cart) {
        if (c.p_by_w == '1') {
          this.p_by_w = true
          break
        }
      }
    } else {
      this.api.setStore('cart', [])
      this.cart = []
    }

  }


  async isOrderActive() {
    var res_id = await 0
    // await this.s.get('restaurant').then((res: any) => {
    //   if (res) {
    //     res_id = res.id_res_auto
    //     var dataAcrive = { res_id: res_id, member_id: this.user.mm_id }
    //     this.api.post('getOrderActiveLastRes', dataAcrive).subscribe((resA: any) => {
    //       this.orderActive = resA
    //     })
    //   }
    // })
  }

  /////////////////////////////////////////////////////////// load defult ///////////////////////////////////////////////////////////
  public payment = 0;
  data: any = {
    logo_url: '',
    name: '',
    address: '',
    id_res_auto: '',
    cover_url: '',
    holiday: [],
    open_time: [],
    send_options: {
      destination: false,
      transfer: false,
      data: []
    }
  };
  public bank: any = [{ id_pay_type: 0 }]
  load_data() {
    let url = this.router['routerState'].snapshot.url;
    let urls = url.split("/");
    this.res = urls[1];
    this.api.get("get_res/" + this.res).subscribe((res: any) => {
      this.data = res.data;
      this.loadDeliveryService()
      if (this.data.send_options.destination == false && this.data.send_options.transfer) {
        this.pay.type = 0;
      }
      this.bank = res.data.bank.length > 0 ? res.data.bank : [{ id_pay_type: 0 }];
      this.liff_init();
    });
  }
  async onReview() {
    const modal = await this.modalCtr.create({
      component: SendReviewComponent,
      backdropDismiss: true,
      cssClass: './review.component.scss'
    })

    modal.onWillDismiss().then((event: any) => {
      //   if (event.role == 'dismiss') {
      this.check_delivery();
      //}
    })

    return await modal.present()
  }
  fb_set = false;
  first = true;

  check_date_time() {
    if (this.data.open_time.length == 0) {
      return { flag: false, at: 'ไม่มีกำหนด' }
    }
    let date = moment().format('YYYY-MM-DD')
    var temp = { flag: true, text: '', at: '' };
    this.data.holiday.forEach(element => {
      if (element.date == date) {
        temp.flag = false;
        temp.text = "วันหยุด " + element.text;
      }
    });
    let day = moment().add(1, 'day').format('D') - 0;
    if (temp.flag) {
      let time = moment().format('HH:mm');
      if (this.data.open_time) {
        this.data.open_time.forEach(element => {
          if (element.day == 8) {
            temp.flag = this.check_time(time, element.time).flag;
            temp.at = this.check_time(time, element.time).at;
          } else if (element.day == 9 && (day == 2 || day == 3 || day == 4 || day == 5 || day == 6)) {
            temp.flag = this.check_time(time, element.time).flag;
            temp.at = this.check_time(time, element.time).at;
          } else if (element.day == 10 && (day == 7 || day == 1)) {
            temp.flag = this.check_time(time, element.time).flag;
            temp.at = this.check_time(time, element.time).at;
          } else if (element.day == day) {
            temp.flag = this.check_time(time, element.time).flag;
            temp.at = this.check_time(time, element.time).at;
          }
        });
      } else {
        temp.flag = false
      }


      if (!temp.flag) {
        temp.text = "ร้านไม่เปิดในวันที่หรือเวลาที่ท่านต้องการจอง";
      }
    }

    return temp;
  }
  check_time(time, times) {
    let temp = { flag: false, at: '' };
    time = moment("2020-01-01 " + time).unix();
    times.forEach(element => {
      let time_start = moment("2020-01-01 " + element.time_start).unix()
      let time_end = moment("2020-01-01 " + element.time_end).unix()
      if (time_start <= time && time_end >= time) {
        temp.flag = true;
        temp.at = element.time_end;
      }
    });
    return temp;
  }
  change() {
    this.status_sp_btn = true
    setTimeout(() => {
      this.deRef.detectChanges();
    }, 200);
  }
  check_delivery() {
    if (this.data.id_res_auto != '') {
      this.api.get("check_delivery/" + this.data.id_res_auto + '/' + this.user.mm_id).subscribe((res: any) => {
        this.status_sp_btn = false
        this.pay.type = 0;
        if (res.status == '5') {
          // window.location.reload();
        }

        setTimeout(() => {
          this.deRef.detectChanges();
        }, 200);

        this.delivery.status = undefined
        if (res.flag) {
          this.show = 2;
          if (!this.fb_set) {
            this.api.fb_get(this.data.id_res_auto + "_" + this.data.code + "/orders/" + res.data.order_id, (va) => {

              // if (!this.first) {
              this.check_delivery();
              // } else {
              //   this.first = false;
              // }
            });
            setTimeout(() => {
              this.fb_set = true;
            }, 100);

          }
          this.delivery = res.data;
          for (let c of this.delivery.items) {
            this.p_by_w = false
            if (c.p_by_w == '1') {
              this.p_by_w = true
              break
            }
          }
          if (this.delivery.status == '1') {
            setTimeout(() => {
              if (this._isSuccess === 0) {
                // this.onReview();
                this._isSuccess++
              }

              this.setDefult();
            }, 1500);
          }
        } else {
          this.fb_set = false;
          this.show = 1;
        }
      });
    } else {
      setTimeout(() => {
        this.check_delivery();
      }, 500);
    }
  }

  totalDelivery(food, send) {
    let total = 0
    if (food) {
      // food.forEach(el => {
      //   total += el.count * el.total_price
      // });
      total += (food + (send - 0))
    }

    return total
  }

  totalFood(food) {
    let total = 0
    if (food) {
      food.forEach(el => {
        total += el.count * el.total_price
        for (let entry of el.details) {
          for (let sub of entry.sub) {
            if (sub.selected) {
              total += parseFloat(sub.price) * el.count;
            }
          }
        }
        for (let entry of el.toppings) {
          if (Number(entry.count) > 0) {
            total += parseFloat(entry.price) * Number(entry.count);
          }
        }
      });
    }

    return total
  }

  async isCart(cart) {
    if (cart.length < 1) {
      await this.setDefult()
      await this.router.navigate([this._restaurant.uri + '/restaurant-menu'])
    }

  }

  _list_ds: any = []
  loadDeliveryService() {
    this._list_ds = []
    this.api.get('loadDeliveryService/' + this.data.id_res_auto).subscribe((res: any) => {
      if (res) {
        res.forEach((el) => {
          let obj = { title: el, checked: false }
          this._list_ds.push(obj)
        });
      }
    })
  }

  getService(e, i) {
    e = e.detail.checked
    this._list_ds.forEach((el, index) => {
      if (index == i) {
        el.checked = e
      }
    });
  }

  /////////////////////////////////////////////////////////// รายการที่สั่ง ///////////////////////////////////////////////////////////

  public total: number = 0
  add_count(item) {
    item.count++;
    this.api.setStore("cart", this.cart)
    this.cfState1 = true
    let my_pos = this.api.getStore('my_pos')
    this.directions(my_pos.lat, my_pos.lng, 0, null)
  }

  sum_all() {
    let sum = 0;
    if (this.cart) {
      this.cart.forEach(element => {
        sum += this.get_price(element);
      });
      if (!this.locationMark.price) {
        this.locationMark.price = 0
      }
      this.total = sum + this.locationMark.price + this._location.price
    }

    return sum;
  }

  count_all() {
    let count = 0;
    this.cart.forEach((val) => {
      count += val.count;
    });
    return count;
  }

  async remove(index) {
    await this.cart.splice(index, 1);
    await this.api.setStore("cart", this.cart)
    let cart = this.api.getStore('cart')
    this.cfState1 = false
    if (cart.length > 0) {
      this.cfState1 = true
    } else {
      this.isCart(cart)
    }
    for (let c of this.cart) {
      this.p_by_w = false
      if (c.p_by_w == '1') {
        this.p_by_w = true
        break
      }
    }
  }
  remove_count(item, i) {
    if (item.count > 1) {
      item.count--;
      this.api.setStore("cart", this.cart)
    } else {
      this.remove(i);
    }
    let my_pos = this.api.getStore('my_pos')
    this.directions(my_pos.lat, my_pos.lng, 0, null)
  }

  async add_food() {

    const modal = await this.modalController.create({
      component: GroupPage,
      cssClass: 'my-custom-class',
      componentProps: {
        id_res_auto: this.data.id_res_auto
      }
    });
    modal.onDidDismiss().then((data: any) => {
      this.s.get('cart').then((food: any) => {
        if (data) {
          this.cart = food;
          this.cfState1 = true
        }
      });
      setTimeout(() => {
        this.check_page();
      }, 1000);
    });
    return await modal.present();
  }


  get_html(c): String {
    let out: String = '';
    for (let entry of c.details) {
      let temp = '';
      let flag = true;
      for (let sub of entry.sub) {
        if (sub.selected) {
          if (flag) {
            flag = false;
            temp += sub.name[0].title;
          } else {
            temp += ", " + sub.name[0].title;
          }
        }
      }
      if (temp != '') {
        out += entry.name[0].title + " : ";
        out += temp;
        out += "<br>";
      }

    }

    for (let entry of c.toppings) {
      if (entry.count - 0 > 0) {
        out += "  [" + entry.count + "] " + entry.title + " (";
        if (entry.price > 0) {
          out += "+";
        }
        out += (entry.price * entry.count) + ")";
        out += "<br>";
      }
    }
    if (c.comments.trim() != '') {
      out += "Comment : " + c.comments;
    }

    return out;
  }

  obj_service = [
    { val: 1, title: 'ตะเกียบ' },
    { val: 2, title: 'ช้อนซ่อม' },
    { val: 3, title: 'น้ำจิ้ม' },
  ]
  public spoon = false
  getSpoon() {
    if (this.spoon) {
      this.spoon = false
    } else {
      this.spoon = true
    }

  }

  /////////////////////////////////////////////////////////// สถานที่ส่ง ///////////////////////////////////////////////////////////


  async editMap() {
    const modal = await this.modalCtrl.create({
      component: MyMapComponent,
      componentProps: {
        data: ''
      },
      backdropDismiss: false,
      cssClass: 'location_page',
    });

    modal.onWillDismiss().then(async (e: any) => {
      if (e.role) {
        await this.api.setStore('my_pos', e.data)
        let my_pos = await this.api.getStore('my_pos')
        await this.directions(my_pos.lat, my_pos.lng, 0, null)
        this._location.price = await e.price
        this._location.text = await e.text
        this._location.distance = await e.distance
        this._location.duration = await e.duration

      }
    })
    return await modal.present();
  }

  public GGM: any;
  send_error = true
  _location: any = { local_name: '-', text: '-', newLocation: '' }
  directions(lat, lng, local_id, local_name) {
    this.send_error = true
    let data_res = this.api.getStore('restaurant')
    this.GGM = new Object(google.maps);
    let directionsService = new this.GGM.DirectionsService();
    let a = data_res.location.split(",");
    let my_Latlng = new this.GGM.LatLng(parseFloat(a[0]), parseFloat(a[1]));
    let initialTo = new this.GGM.LatLng(lat, lng);
    var request = {
      origin: my_Latlng,
      destination: initialTo,
      travelMode: this.GGM.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, (results, status) => {
      var can_send = data_res.send_options.typeLowPrice
      var distance = parseInt(results.routes[0].legs[0].distance.value) / 1000
      var send: any = { text: 'ระยะทาง 0 กม. จัดส่งประมาณ 0 นาที', local_id: null, mark_name: null, price: 0, distance: 0, duration: 0, error: true }
      var sum_price = this.sum_all()
      var index = 0
      var sum_start = 0
      if (can_send) {
        for (let cs of can_send) {
          var next_obj = can_send[index + 1] != undefined ? can_send[index + 1] : can_send[index]
          // if (sum_price > sum_start && sum_price <= cs.sum_price)
          if (sum_price >= cs.sum_price && sum_price <= next_obj.sum_price) {
            for (let dp of cs.data) {
              if (distance <= dp.number) {
                this.api.setStore('location_tmp', { lat: lat, long: lng, local_id: local_id })
                send = {
                  text: 'ระยะทาง ' + results.routes[0].legs[0].distance.text + ' จัดส่งประมาณ ' + results.routes[0].legs[0].duration.text,
                  local_id: local_id,
                  local_name: results.routes[0].legs[0].end_address,
                  end_address: results.routes[0].legs[0].end_address,
                  price: dp.price,
                  distance: results.routes[0].legs[0].distance,
                  duration: results.routes[0].legs[0].duration,
                  lat: lat,
                  long: lng,
                  error: false,
                  newLocation: this._location.newLocation
                }
                this.send_error = false
                break
              }
            }
            break
          } else {
            if (can_send.length - 1 == index) {
              for (let dp of cs.data) {
                if (distance <= dp.number) {
                  this.api.setStore('location_tmp', { lat: lat, long: lng, local_id: local_id })
                  send = {
                    text: 'ระยะทาง ' + results.routes[0].legs[0].distance.text + ' จัดส่งประมาณ ' + results.routes[0].legs[0].duration.text,
                    local_id: local_id,
                    local_name: results.routes[0].legs[0].end_address,
                    end_address: results.routes[0].legs[0].end_address,
                    price: dp.price,
                    distance: results.routes[0].legs[0].distance,
                    duration: results.routes[0].legs[0].duration,
                    lat: lat,
                    long: lng,
                    error: false,
                    newLocation: this._location.newLocation
                  }
                  this.send_error = false
                  break
                }
              }
            } else {
              sum_start = cs.sum_price
            }
          }
          index++
        }
      } else {
        this.api.setStore('location_tmp', { lat: lat, long: lng, local_id: local_id })
        send = {
          text: 'ระยะทาง ' + results.routes[0].legs[0].distance.text + ' จัดส่งประมาณ ' + results.routes[0].legs[0].duration.text,
          local_id: local_id,
          local_name: results.routes[0].legs[0].end_address,
          end_address: results.routes[0].legs[0].end_address,
          price: 0,
          distance: results.routes[0].legs[0].distance,
          duration: results.routes[0].legs[0].duration,
          lat: lat,
          long: lng,
          error: false,
          newLocation: this._location.newLocation
        }
        this.send_error = false
      }

      if (send.error) {
        this.api.Toast('เกินระยะจัดส่ง!')
        this.cfState2 = false
      } else {
        this._location = send
        this.cfState2 = true
      }
    });
  }

  /////////////////////////////////////////////////////////// ยอดชำระ ///////////////////////////////////////////////////////////

  status_sp_btn = false
  paymentSelect(type_transfer) {
    console.log(this.bank);

    this.status_sp_btn = true
    let type = type_transfer
    this.cfState3 = true
    this.src = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
    this.pay = { type: 0, id_pay_type: undefined }
    if (type == 1) {
      this.pay = { type: 1, id_pay_type: this.bank[0].id_pay_type }
      this.cfState3 = false
    }
    setTimeout(() => {
      this.deRef.detectChanges()
    }, 200);
  }

  copy(number) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = number;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.api.Toast('คัดลอกเลขที่บัญชีเรียบร้อย')
  }

  bankSelect(e) {
    this.pay = {
      type: 1,
      id_pay_type: e.detail.value
    }
    for (let b of this.bank) {
      if (b.id_pay_type == e.detail.value) {
        this.copy(b.acount_number)
        break
      }
    }
  }

  public src: any = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
  changeFile(e) {
    var input = e.target
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (el) => {
        this.src = {
          path: el.target.result,
          name: input.files[0].name,
          state: true
        }
        this.cfState3 = true
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    } else {
      this.src = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
      this.cfState3 = false
    }

    setTimeout(() => {
      this.deRef.detectChanges()
    }, 500);
  }


  async confirmOrder(type) {
    var text = type == 'cf' ? 'สั่งอาหาร?' : 'ชำระเงินเรียบร้อย?'
    const alert = await this.alert.create({
      mode: 'ios',
      cssClass: 'my-custom-class',
      header: text,
      message: 'กด "ยืนยัน" หากทำรายการแล้วเรียบร้อย',
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel'
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.setOrder(type)

          }
        }
      ]
    });
    await alert.present();
  }

  async liff_init() {
    await liff.init({ liffId: this.data.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f"
      };
      this.api.post("update_profile", data).subscribe(async (res: any) => {
        this.user = await res;
        this.check_delivery();
        this.api.fb_get(this.data.id_res_auto + "_" + this.data.code + "/data_pay", () => {
          this.check_delivery();
        });
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {
      await this.getUserProfile();
    } else {
      // liff.login({ redirectUri: window.location.href })
    }
  }
  async setOrder(type) {


    var user: any = []
    var payType: any = []

    user = await this.user
    payType = await this.pay

    var tmp_s = []
    this._list_ds.forEach(el => {
      if (el.checked) {
        tmp_s.push(el.title)
      }
    });

    var order: any = await {
      user: this.user,
      res: this.data.id_res_auto,
      cart: this.cart,
      delivery_service: tmp_s,
      spoon: this.spoon,
      local: this._location,
      payType: payType,
      slip: this.src.path,
      total: this.total,
      delivery_contact: { c_name: this.c_name, c_phone: this.c_phone }
    }

    if (type == 'pm') {
      this.cfState1 = true
    }

    if (type == 'cf' && this.cfState1 && this.cfState2) {
      this.api.setStore('c_name', this.c_name)
      this.api.setStore('c_phone', this.c_phone)
      this.api.post('addOrder', order).subscribe((res: any) => {
        if (res.status == 'success') {
          this.setDefult();
          this.api.fb_setAuto(this.data.id_res_auto + "_" + this.data.code + "/data_pay");
          this.api.fb_setManual(this.data.id_res_auto + "_" + this.data.code + "/new_delivery", res.order_id);
          this.api.fb_setManual(this.data.id_res_auto + "_" + this.data.code + "/new_delivery_noti", res.order_id);
          this.check_delivery();
        }
      })
    } else if (type == 'pm' && this.cfState2 && this.cfState3) {

      order.order_id = this.delivery.order_id
      order.total = this.totalDelivery(this.price_total, this.delivery.price_send)

      this.api.post('updatepaymentOrder', order).subscribe((res: any) => {
        if (res.status == 'success') {
          this.api.fb_setAuto(this.data.id_res_auto + "_" + this.data.code + "/data_pay");
          this.api.fb_setManual(this.data.id_res_auto + "_" + this.data.code + "/new_delivery", this.delivery.order_id);
          this.api.fb_setManual(this.data.id_res_auto + "_" + this.data.code + "/new_delivery_noti_payment", res.order_id);
          this.check_delivery();
        }
      })
    } else {
      var valid = [
        { value: this.cfState1, txt: 'โปรดเพิ่มรายการอาหาร' },
        { value: this.cfState2, txt: 'โปรดระบุสถานที่ส่ง' },
        { value: this.cfState3, txt: 'โปรดแนบหลักฐานการโอนเงิน' },
      ]
      valid.forEach(async (el) => {
        if (!el.value) {
          const alert = await this.alert.create({
            mode: 'ios',
            cssClass: 'my-custom-class',
            header: 'ผิดผลาด!',
            message: el.txt,
            buttons: [
              {
                text: 'ตกลง',
                handler: () => {
                }
              }
            ]
          });
          await alert.present();
          return false
        }
      });
    }
  }

  async setDefult() {
    await this.api.setStore('cart', [])
    this._location = await { local_name: '-', text: '-', newLocation: '' }
    this.src.path = await ''
    this.cfState1 = false
    this.p_by_w = false
    this.cart = []
    if (this.spoon) {
      var checkbox = document.getElementById('needSpoon')
      checkbox.click()
    }

    // var destination = document.getElementById('destination')
    // destination.click()
    this.src.path = ''

  }
  //////////////////////////////success//////////////

  price_total: number = 0
  get_price(c): number {
    let out = 0;
    if (c.is_buffet - 0 == 1) {
      out = 0;
    } else {
      out = parseFloat(c.price);
    }

    for (let entry of c.details) {
      for (let sub of entry.sub) {
        if (sub.selected) {
          out += parseFloat(sub.price);
        }
      }
    }
    for (let entry of c.toppings) {
      if (Number(entry.count) > 0) {
        out += parseFloat(entry.price) * Number(entry.count);
      }
    }
    this.price_total = out * Number(c.count);
    return out * Number(c.count);

    return out
  }

  priceTotal(food) {
    console.log(food);

  }



  public directionsService = new google.maps.DirectionsService();
  public directionsRenderer = new google.maps.DirectionsRenderer();
  @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;

  initMap(): void {

    const map = new google.maps.Map(this.mapNativeElement.nativeElement,
      {
        zoom: 13,
        center: { lat: 37.77, lng: -122.447 },
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_CENTER,
        },
        zoomControl: false,
        zoomControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER,
        },
        scaleControl: false,
        streetViewControl: false,
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP,
        },
        fullscreenControl: false,

      }
    );
    this.directionsRenderer.setMap(map);

  }

  cooking() {
    let flag = false;
    this.delivery.items.forEach(element => {
      if (element.status != '0') {
        flag = true;
      }
    });
    return flag;
  }

  finish() {
    let flag = true;
    this.delivery.items.forEach(element => {
      if (element.status == '0' || element.status == '1') {
        flag = false;
      }
    });
    return flag;
  }
  connect_fb() {
    if (this.data.id_res_auto && this.delivery.order_id) {
      let key = this.data.id_res_auto + "_" + this.data.code + "/rider/" + this.delivery.order_id;
      this.api.fb_get(key, (val) => {
        if (val.val()) {
          this.directionsService.route(
            {
              origin: { lat: val.val().lat, lng: val.val().lng }, // Haight.
              destination: { lat: this.delivery.location.lat, lng: this.delivery.location.lng },///ลูกค้าอยู่
              // travelMode: google.maps.TravelMode['DRIVING'],
              optimizeWaypoints: true,
              travelMode: google.maps.TravelMode.DRIVING,

            },
            (response, status) => {
              if (status == "OK") {
                this.directionsRenderer.setDirections(response);
              } else {
                window.alert("Directions request failed due to " + status);
              }
            }
          );
          this.rider_send = true;
        } else {
          this.rider_send = false;
        }
      })
    } else {
      setTimeout(() => {
        this.connect_fb();
      }, 1000);
    }
  }

  async cancelOrder() {
    const alert = await this.alert.create({
      mode: 'ios',
      cssClass: 'my-custom-class',
      header: 'ยกเลิกการสั่ง?',
      message: 'กด "ยืนยัน" หากต้องการยกเลิกการทำรายการ',
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel'
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.api.post('cancelOrder', { order_id: this.delivery.order_id }).subscribe((res: any) => {
              if (res.status == 'success') {
                this.setDefult()
                this.api.fb_setAuto(this.data.id_res_auto + "_" + this.data.code + "/data_pay");
                this.api.fb_setManual(this.data.id_res_auto + "_" + this.data.code + "/new_delivery", this.delivery.order_id);
                this.loadCart()
                this.check_delivery();
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }
}
