import { ActivatedRoute, Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
declare var liff: any;

@Component({
  selector: 'app-queue',
  templateUrl: './queue.page.html',
  styleUrls: ['./queue.page.scss'],
})
export class QueuePage implements OnInit {
  res: any;
  data: any = {};
  queue = [];
  select_show = true;
  data_res: any = {};
  constructor(public route: ActivatedRoute, public router: Router,
    private chRef: ChangeDetectorRef,
    public api: ApiServiceService, public alertController: AlertController, public s: Storage) { }

  ngOnInit() {
    let url = this.router['routerState'].snapshot.url;
    let urls = url.split("/");
    this.res = urls[1];

    this.api.get("get_res/" + this.res).subscribe((res: any) => {
      this.data_res = res.data;
      this.api.fb_get(this.data_res.id_res_auto + "_" + this.data_res.code + "/queue", () => {
        if (this.select_show) {
          this.load_queue();
        } else {
          this.load_my_queue();
        }
      });
    });

  }
  ionViewWillEnter() {
    let url = this.router['routerState'].snapshot.url;
    let urls = url.split("/");
    console.log(urls);
    this.res = urls[1];
    this.load_data();
  }

  public user: any = {}
  public localhost = window.location.href.indexOf("localhost") != -1;
  async liff_init() {
    await liff.init({ liffId: this.data_res.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f",
        'id_res_auto': this.data.id_res_auto
      };
      this.api.post("update_profile", data).subscribe((res) => {
        console.log(res);
        this.user = res;
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {

      this.getUserProfile();
    } else {
      // liff.login({ redirectUri: window.location.href })
    }
  }

  async getUserProfile() {
    const profile = await liff.getProfile();
    profile.id_res_auto = this.data.id_res_auto;
    this.api.post("update_profile", profile).subscribe((res) => {
      this.user = res;
    });
  }

  async cancel_q() {
    console.log("cancel");
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'ยืนการยกเลิกคิว',
      // message: '',
      buttons: [
        {
          text: 'ปิดออก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            console.log('Confirm Okay');
            this.api.post("cancel_q", { q_id: this.data.q_id }).subscribe((res) => {
              this.api.fb_setAuto(this.data_res.id_res_auto + "_" + this.data_res.code + "/queue");
              this.load_data();
            })
          }
        }
      ]
    });
    await alert.present();
  }
  load_data() {
    this.api.get("get_res/" + this.res).subscribe((res: any) => {
      console.log(res);
      this.data_res = res.data;
      this.liff_init()
      this.api.get('check_queue/' + this.user.mm_id + "/" + this.data_res.id_res_auto).subscribe((result: any) => {
        this.select_show = !result.flag;
        if (this.select_show) {
          this.load_queue();
        } else {
          this.load_my_queue();
        }
      });
    })
  }
  async res_cancel() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'คิวของคณถูกยกเลิกแล้ว',
      // subHeader: 'Subtitle',
      // message: '',
      buttons: ['OK']
    });

    await alert.present();
  }
  doRefresh(e) {
    this.load_data();
    setTimeout(() => {
      e.target.complete();
    }, 1000);
  }
  async edit_people() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'จำนวนคน',
      inputs: [
        {
          name: 'number',
          type: 'number',
          value: this.data.people,
          placeholder: 'จำนวนคน'
        },

      ],
      buttons: [
        {
          text: 'ปิดออก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'บันทึก',
          handler: (val) => {
            console.log(val);
            this.api.post("update_people", { number: val.number, 'q_id': this.data.q_id }).subscribe((res) => {
              console.log(res);
              this.api.fb_setAuto(this.data_res.id_res_auto + "_" + this.data_res.code + "/queue");

            })
          }
        }
      ]
    });

    await alert.present();
  }
  load_my_queue() {
    this.api.get("get_my_queue/" + this.data_res.id_res_auto + "/" + this.user.mm_id).subscribe((res: any) => {

      this.data = res;
      if (this.data.status == '3') {
        // this.open_table();
        window.location.href = "https://develop.deltafood.co/ci/t/" + this.data.url;
      } else if (this.data.status == '2') {
        this.load_data();
        this.res_cancel();

      }
      this.chRef.detectChanges();
    });
  }
  load_queue() {
    this.api.get("get_queue/" + this.data_res.id_res_auto).subscribe((res: any) => {
      console.log(res);
      this.queue = res;
      this.chRef.detectChanges();
    });
  }
  open_table() {
    window.location.href = "https://develop.deltafood.co/ci/t/" + this.data.url;
  }

  async select_queue(queue) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'จำนวนที่นั่ง',
      inputs: [
        {
          name: 'people',
          type: 'number',
          placeholder: 'จำนวนที่นั่ง'
        }
      ],
      buttons: [
        {
          text: 'ปิดออก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ยืนยัน',
          handler: (val) => {
            this.api.post("add_queue", { queue: queue, number: val.people, user: this.user, id_res_auto: this.data_res.id_res_auto }).subscribe((res: any) => {
              console.log(res);
              this.load_data();
              this.api.fb_setAuto(this.data_res.id_res_auto + "_" + this.data_res.code + "/queue");
            });
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

}
