import { ActivatedRoute, Router } from '@angular/router';
import { ChangeDetectorRef, Component } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ApiServiceService } from '../api-service.service';
import { Storage } from '@ionic/storage';
import { GroupPage } from '../group/group.page';
import * as moment from 'moment';
import { interval } from 'rxjs';
declare var liff: any;
@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})
export class Tab4Page {
  public cart: any = [];
  res = '';
  note = '';
  user: any = { mm_id: '' };
  show = 0;
  booking_data: any = { booking_contact: { c_name: '', c_phone: '' } };
  public date = moment().format('YYYY-MM-DD')
  public time = moment.utc(0 * 1000).format('HH:mm')
  public bank: any = [{ id_pay_type: 0 }]
  public data: any = {
    line_settings: { liff_delivery: '' },
    is_open: { at: '' },
    booking: { zones: '' }
  };
  public localhost = window.location.href.indexOf("localhost") != -1;
  constructor(
    public modalCtrl: ModalController,
    public api: ApiServiceService,
    public storage: Storage,
    public alertController: AlertController,
    public modalController: ModalController,
    public alert: AlertController,
    private ref: ChangeDetectorRef,
    public rounte: ActivatedRoute,
    public router: Router,
  ) {
    console.log(moment.utc(0 * 1000).format('HH:mm'));

  }

  ionViewDidEnter() {
    this.cart = this.api.getStore('cart')
  }

  async cancel() {
    let message = 'คุณจะไม่ได้รับเงินมัดจำคืน';
    if (this.booking_data.total == '0') {
      message = '';
    }
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'ยืนยันการยกเลิก',
      message: message,
      buttons: [
        {
          text: 'ปิดออก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            this.api.post("cancel_booking", { booking_id: this.booking_data.booking_id, id_res_auto: this.data.id_res_auto }).subscribe((res: any) => {
              if (res == '1') {
                this.cart = []
                this.cart = this.api.setStore('cart', this.cart)
                this.load_data()
                liff.sendMessages([
                  {
                    type: 'text',
                    text: 'ยกเลิกการจอง'
                  }
                ]).then(() => {
                  console.log('message sent');


                }).catch((err) => {
                  console.log('error', err);
                });

                let text = this.data.id_res_auto + "_" + this.data.code + "/booking";
                this.api.fb_setAuto(text);
                this.check_user_booking();
              }
            });
          }
        }
      ]
    });

    await alert.present();
  }
  async liff_init() {
    await liff.init({ liffId: this.data.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f"
      };
      this.api.post("update_profile", data).subscribe((res) => {
        this.user = res;
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {
      this.getUserProfile();
    } else {
      // liff.login({ redirectUri: window.location.href })
    }
  }

  ngOnInit(): void {

    this.load_data();
    setTimeout(() => {
      this.setContact()
    }, 1500);
  }

  c_name: string = ''
  c_phone: string = ''
  setContact() {
    this.c_name = this.api.getStore('c_name') ? this.api.getStore('c_name') : this.user.mm_name
    this.c_phone = this.api.getStore('c_phone') ? this.api.getStore('c_phone') : ''
  }


  async getUserProfile() {
    const profile = await liff.getProfile();
    this.api.post("update_profile", profile).subscribe((res) => {

      this.user = res;

    });

  }

  ////////////////////////////////////////////////////////////// load defult //////////////////////////////////////////////////////////////
  min_date = moment().format('YYYY-MM-DD')
  max_date = moment().format('YYYY-MM-DD')
  load_data() {
    let url = this.router['routerState'].snapshot.url;
    let urls = url.split("/");
    this.res = urls[1];
    this.api.get("get_res/" + this.res).subscribe((res: any) => {
      this.checkZone()
      this.data = res.data;
      this.setDateTimePick()
      this.tempZones = this.data.booking.zones
      this.text_open = { is_open: this.check_date_time(this.date + " " + this.time).flag, txt: this.check_date_time(this.date + " " + this.time).text }
      // if (this.data.booking.zones) {
      //   this.zone = { b_id: this.data.booking.zones[0].b_id, price: this.data.booking.zones[0].price }
      // }
      this.bank = res.data.bank
      console.log(this.bank);

      if (this.bank) {
        this.bank_id = this.bank[0].id_pay_type
      }

      if (this.data.booking.zones) {
        this.zone = { b_id: this.data.booking.zones[0].b_id, price: this.data.booking.zones[0].price }
      }

      this.booking = res.data.booking.min;
      let text = this.data.id_res_auto + "_" + this.data.code + "/booking";
      this.api.fb_get(text, (res) => {
        this.check_user_booking();
      })
      this.liff_init();

    })
  }

  not_ready_time: boolean = true
  setDateTimePick() {
    this.not_ready_time = false
    this.max_date = moment().add(this.data.booking.max, 'days').format('YYYY-MM-DD')
    this.time = this.data.booking.timeS
    let sec_tmin = moment(this.data.booking.timeS, 'HH:mm').diff(moment().startOf('day'), 'seconds')
    let sec_tmax = moment(this.data.booking.timeE, 'HH:mm').diff(moment().startOf('day'), 'seconds')
    let sec_tn = moment(moment(), 'HH:mm').diff(moment().startOf('day'), 'seconds')
    console.log(this.data);
    if (sec_tn >= sec_tmin && sec_tn <= sec_tmax) {
      let t_tmp = moment().add(this.data.booking.min, 'hours').format('HH:mm')
      let tn_tmp = moment(t_tmp, 'HH:mm').diff(moment().startOf('day'), 'seconds')
      if (tn_tmp > sec_tmax) {
        this.date = moment().add(1, 'days').format('YYYY-MM-DD')
        this.min_date = moment().add(1, 'days').format('YYYY-MM-DD')
      } else {
        let t_obj = t_tmp.split(":")
        t_obj[1] = parseInt(t_obj[1]) < 30 ? '00' : '30'
        this.time = t_obj[0] + ':' + t_obj[1]
        this.data.booking.timeS = this.time
      }
    } else if (sec_tn > sec_tmin && sec_tn > sec_tmax) {
      this.date = moment().add(1, 'days').format('YYYY-MM-DD')
      this.min_date = moment().add(1, 'days').format('YYYY-MM-DD')
    } else {
      this.not_ready_time = true
    }
  }

  check_user_booking() {
    if (this.user.mm_id != '') {
      this.api.get("eheck_booking/" + this.data.id_res_auto + "/" + this.user.mm_id).subscribe((res: any) => {
        if (res.flag) {
          this.show = 2;
          this.booking_data = res.data;
        } else {
          this.show = 1;
        }
        this.ref.detectChanges();
      });
    } else {
      setTimeout(() => {
        this.check_user_booking();
      }, 500);

    }

  }

  tempZones: any = []
  checkZone() {
    setTimeout(() => {
      let time_now = moment.duration(this.time).asSeconds()
      let timeS = moment.duration(this.data.booking.timeS).asSeconds()
      let timeE = moment.duration(this.data.booking.timeE).asSeconds()
      if (time_now < timeS || time_now > timeE) {
        this.data.booking.zones = []
        this.ref.detectChanges()
      } else {
        this.data.booking.zones = this.tempZones
        this.ref.detectChanges()
      }
    }, 0);

  }

  ////////////////////////////////////////////////////////////// queue time //////////////////////////////////////////////////////////////
  public cfState1: boolean = true
  public booking: any = {};
  public text_open: any = { is_open: false, txt: 'ร้านไม่เปิดในวันที่หรือเวลาที่ท่านต้องการจอง1' }
  public zone: any = { b_id: 0, price: 0 }
  bookingChange() {
    this.booking = parseInt(this.booking)
    if (this.booking < this.data.booking.min || this.booking == '') {
      this.booking = this.data.booking.min
    } else if (this.booking > this.data.booking.max) {
      this.booking = this.data.booking.max
    }
  }

  dateChange() {
    this.check_date_time(moment(this.date).format('YYYY-MM-DD') + " " + this.time)
  }
  public d: any = {};
  check_booking(date_time) {
    // let date_t = date_time.split(" ");
    // let date = date_t[0].split("-");
    // let h = date_t[1].split(":");
    // let d = new Date(date[0], date[1], date[2], h[0], h[1]);
    // let temp = { flag: true, text: '', at: '' };
    // let min = new Date();
    // let max = new Date();
    // min.setHours(min.getHours() + this.data.booking.min);
    // max.setDate(max.getDate() + this.data.booking.max);
    // this.d.d_time = d.getTime();
    // this.d.min_time = min.getTime();
    // this.d.max_time = max.getTime();
    // if (d.getTime() >= min.getTime() && d.getTime() <= max.getTime()) {
    //   temp = this.check_date_time(date_time);
    // } else {
    //   temp.flag = false;
    //   temp.text = "วันเวลาจองเร็วหรือนานเกินไป";
    // }
    // return temp;


    let temp = { flag: true, text: '', at: '' };
    if (this.not_ready_time) {
      temp.flag = false;
      temp.text = "วันเวลาจองเร็วหรือนานเกินไป";
    }
    return temp;
  }

  check_time(time, times) {
    let temp = { flag: false, at: '' };
    let date: any = this.date.split("-");
    let h = time.split(":");
    time = new Date(date[0], date[1], date[2], h[0], h[1]);
    times.forEach(element => {
      date = this.date.split("-");
      h = element.time_start.split(":");
      let time_start = new Date(date[0], date[1], date[2], h[0], h[1]);
      h = element.time_end.split(":");
      let time_end = new Date(date[0], date[1], date[2], h[0], h[1]);
      if (time_start.getTime() <= time.getTime() && time_end.getTime() >= time.getTime()) {
        temp.flag = true;
        temp.at = element.time_end;
      }
    });
    return temp;
  }
  check_date_time(date_time) {
    let date_t = date_time.split(" ");
    let date2: any = date_t[0].split("-");
    let h = date_t[1].split(":");
    let d = new Date(date2[0], date2[1], date2[2], h[0], h[1]);

    let date = d.getFullYear() + "-" + (('0' + (d.getMonth() + 1)).slice(-2)) + "-" + (('0' + d.getDate()).slice(-2));
    let temp = { flag: true, text: 'เลือกวันเวลาการจอง', at: '' };
    this.data.holiday.forEach(element => {
      if (element.date == date) {
        temp.flag = false;
        temp.text = "วันหยุด " + element.text;
      }
    });
    let day = d.getDay() + 1;
    if (temp.flag) {
      let time = d.getHours() + ":" + d.getMinutes();
      this.data.open_time.forEach(element => {
        if (element.day == 8) {
          temp.flag = this.check_time(time, element.time).flag;
          temp.at = this.check_time(time, element.time).at;
        } else if (element.day == 9 && (day == 2 || day == 3 || day == 4 || day == 5 || day == 6)) {
          temp.flag = this.check_time(time, element.time).flag;
          temp.at = this.check_time(time, element.time).at;
        } else if (element.day == 10 && (day == 7 || day == 1)) {
          temp.flag = this.check_time(time, element.time).flag;
          temp.at = this.check_time(time, element.time).at;
        } else if (element.day == day) {
          temp.flag = this.check_time(time, element.time).flag;
          temp.at = this.check_time(time, element.time).at;
        }
      });
      this.text_open = { is_open: true, txt: 'ร้านเปิดถึง ' + temp.at }
      if (!temp.flag) {
        temp.text = "ร้านไม่เปิดในวันที่หรือเวลาที่ท่านต้องการจอง";
        this.text_open = { is_open: false, txt: temp.text }
      }
      this.checkZone()
    }
    return temp;
  }

  getZone() {
    this.data.booking.zones.forEach((el: any) => {
      if (this.zone.b_id == el.b_id) {
        this.zone = { b_id: el.b_id, price: el.price }
        // this.total = parseInt(el.price) + this.total
      }
    });
  }

  ////////////////////////////////////////////////////////////// food //////////////////////////////////////////////////////////////

  async add_food() {

    // const modal = await this.modalController.create({
    //   component: GroupPage,
    //   cssClass: 'my-custom-class',
    //   componentProps: {
    //     id_res_auto: this.data.id_res_auto
    //   }
    // });
    // modal.onDidDismiss().then((data: any) => {
    //   this.storage.get('cart').then((food: any) => {
    //     if (data) {
    //       this.cart = food;
    //     }
    //   });
    // });
    // return await modal.present();
  }


  public total = 0
  add_count(item) {
    item.count++;
    this.api.setStore("cart", this.cart);
  }

  sum_all() {
    let sum = 0;
    if (this.cart) {
      this.cart.forEach(element => {
        sum += this.get_price(element);
      });
    }

    this.total = sum + parseInt(this.zone.price)
    return sum;
  }

  count_all() {
    let count = 0;
    this.cart.forEach((val) => {
      count += val.count;
    });
    return count;
  }

  async remove(index) {
    await this.cart.splice(index, 1);
    await this.api.setStore('cart', this.cart)
  }
  remove_count(item, i) {
    if (item.count > 1) {
      item.count--;
      this.api.setStore('cart', this.cart)
    } else {
      this.remove(i);
    }
  }

  get_html(c): String {
    let out: String = '';
    for (let entry of c.details) {
      // let price = 0;
      let temp = '';
      let flag = true;
      for (let sub of entry.sub) {
        if (sub.selected) {
          if (flag) {
            flag = false;
            temp += sub.name[0].title;
          } else {
            temp += ", " + sub.name[0].title;
          }
        }
      }
      if (temp != '') {
        out += entry.name[0].title + " : ";
        out += temp;
        out += "<br>";
      }

    }

    for (let entry of c.toppings) {
      if (entry.count - 0 > 0) {
        out += "  [" + entry.count + "] " + entry.title + " (";
        if (entry.price > 0) {
          out += "+";
        }
        out += (entry.price * entry.count) + ")";
        out += "<br>";
      }
    }
    if (c.comments.trim() != '') {
      out += "Comment : " + c.comments;
    }

    return out;
  }
  get_price(c): number {
    let out = 0;
    if (c.is_buffet - 0 == 1) {
      out = 0;
    } else {
      out = parseFloat(c.price);
    }

    for (let entry of c.details) {
      for (let sub of entry.sub) {
        if (sub.selected) {
          out += parseFloat(sub.price);
        }
      }
    }
    for (let entry of c.toppings) {
      if (Number(entry.count) > 0) {
        out += parseFloat(entry.price) * Number(entry.count);
      }
    }
    return out * Number(c.count);
  }

  public spoon = false
  getSpoon() {
    if (this.spoon) {
      this.spoon = false
    } else {
      this.spoon = true
    }
  }

  ////////////////////////////////////////////////////////////// payment //////////////////////////////////////////////////////////////
  public cfState: boolean = false

  public bank_id: any = 0
  bankSelect(e) {
    this.bank_id = e.detail.value
  }

  public src: any = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
  changeFile(e) {
    var input = e.target
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (el) => {
        this.src = {
          path: el.target.result,
          name: input.files[0].name,
          state: true
        }
        this.cfState = true
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    } else {
      this.src = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
      this.cfState = false
    }
  }

  async confirmOrder() {
    const alert = await this.alert.create({
      mode: 'ios',
      cssClass: 'my-custom-class',
      header: 'ยืนยัน!',
      message: 'ยืนยันการจองโต๊ะ.',
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel'
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            var valid = this.validatorState()
            if (valid.val) {
              this.setOrder()
            } else {
              setTimeout(async () => {
                const alertFail = await this.alert.create({
                  mode: 'ios',
                  cssClass: 'my-custom-class',
                  header: 'ผิดผลาด!',
                  message: valid.txt,
                  buttons: [
                    {
                      text: 'ตกลง'
                    }
                  ]
                });
                await alertFail.present();
              }, 0);
            }

          }
        }
      ]
    });
    await alert.present();
  }

  validatorState() {
    var valid = { val: true, txt: '' }
    var booking = this.check_booking(this.date + " " + this.time + ":00 UTC");
    if (this.total != 0) {
      var data = [
        { val: booking.flag, txt: booking.text },
        { val: this.cfState, txt: 'โปรดแนบหลักฐานการโอน' },
      ]
      for (let el of data) {
        if (!el.val) {
          valid = el
          break
        }
      }
    } else {
      valid = { val: booking.flag, txt: booking.text }
    }
    return valid
  }

  setOrder() {
    var order = {
      user: this.user,
      queue: {
        date: this.date,
        time: this.time,
        booking: this.booking,
        zone: this.zone,
        note: this.note,
        empty_quere: this.booking.limit_queue - 1
      },
      id_res_auto: this.data.id_res_auto,
      food: this.cart,
      payment: { bank_id: this.bank_id, slip: this.src },
      total: this.total,
      booking_contact: { c_name: this.c_name, c_phone: this.c_phone }
    }
    this.api.post('addBooking', order).subscribe(async (res: any) => {
      this.cart = await [];
      await this.storage.set("cart", this.cart);
      if (res > 0) {
        let text = await this.data.id_res_auto + "_" + this.data.code + "/add_booking";
        await this.api.fb_setAuto(text);
        await this.check_user_booking();
        await this.load_data()
      } else {
      }
    })
  }
}


