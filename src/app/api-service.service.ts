import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ToastController } from '@ionic/angular';
declare var liff: any;

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  // configUrl = "https://delivery.deltafood.me/api/index.php/Delivery/";
  configUrl = "https://develop.deltafood.me/api/index.php/Delivery/";
  constructor(private http: HttpClient, public toastController: ToastController, public firebase: AngularFireDatabase) { }
  get(uri) {

    return this.http.get(this.configUrl + uri + "?lang=th")

  }

  post(uri, data) {
    return this.http.post(this.configUrl + uri, data);
  }

  get_url(url) {
    return this.http.get(url);

  }

  fb_get(node, func) {
    this.firebase.database.ref(node).on("value", (val: any) => {
      func(val);
    });
  }

  fb_setAuto(node) {
    this.firebase.database.ref(node).set(Math.ceil(Math.random() * 100));
  }

  fb_setManual(node, val) {
    this.firebase.database.ref(node).set(val);
  }

  setStore(key, val) {
    localStorage.setItem(key, JSON.stringify(val))
  }

  getStore(key) {
    return JSON.parse(localStorage.getItem(key))
  }

  async Toast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  public localhost = window.location.href.indexOf("localhost") != -1;
  async liff_init() {
    let restaurant: any = this.getStore('restaurant')
    await liff.init({ liffId: restaurant.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f"
      };
      this.post("update_profile", data).subscribe(async (res) => {
        this.setStore('user', res)
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {
      await this.getUserProfile();
    } else {
      // liff.login({ redirectUri: window.location.href })
    }
  }

  async getUserProfile() {
    const profile = await liff.getProfile();
    this.post("update_profile", profile).subscribe((res) => {
      this.setStore('user', res)
    });
  }


}
