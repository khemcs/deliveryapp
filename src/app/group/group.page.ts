import { ProductListPage } from './../product-list/product-list.page';
import { Component, OnInit, Input } from '@angular/core';
// import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.page.html',
  styleUrls: ['./group.page.scss'],
})
export class GroupPage implements OnInit {
  public group = [];

  public name = '';
  @Input() id_res_auto = '';
  constructor(
    public api: ApiServiceService,
    public storage: Storage,
    public modalController: ModalController) { }

  ngOnInit() {
    // console.log(this.id_res_auto);
    // this.storage.get("group" + this.id_res_auto).then((group) => {
    //   if (group) {
    //     this.group = group;
    //   }
    // })
    this.api.get('load_group/' + this.id_res_auto).subscribe((res: any) => {
      console.log(res);
      this.group = res.data;
      // // this.storage.set("group" + this.id_res_auto, res);
      // console.log(this.group);
    })
  }

  async select_group(g) {
    console.log(g);
    const modal = await this.modalController.create({
      component: ProductListPage,
      cssClass: 'my-custom-class',
      componentProps: {
        fg_id: g.fg_id,
        name: g.name
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if (data.data.close) {
        setTimeout(() => {
          this.modalController.dismiss({ close: true });
        }, 500);
      }
    });
    return await modal.present();
  }
  back() {
    this.modalController.dismiss();
  }

}
