import { ApiServiceService } from './../../api-service.service';
import { Component, Input, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {

  @Input() img_review_url: any = []
  constructor(public api: ApiServiceService, private store: Storage) { 
  }

  ngOnInit() {
    setTimeout(() => {
    }, 1000);
  }
}
