import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { ApiServiceService } from 'src/app/api-service.service';
import { SendReviewComponent } from './send-review/send-review.component';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit {

  // @Input() res_id
  res_id : any = 0
  @Input() user: any = { mm_id: 0 }
  constructor(private api: ApiServiceService, private store: Storage,
    private modalCtr: ModalController,
    private alert: AlertController) { }

  ngOnInit() {
    setTimeout(async () => {
      // await this.loadUser()
      this.loadRestaurant()
      await this.loadReview()
    }, 500);
  }

  loadRestaurant() {
    let res = this.api.getStore('restaurant')
    if (res) {
      this.res_id = res.id_res_auto
    } else {
      this.loadRestaurant()
    }
  }

  public reviews: any = [];
  loadReview() {
    this.api.get('get_review/' + this.res_id).subscribe((resr: any) => {
      this.reviews = resr;
    })
  }

  // public user: any = { mm_id: 0 }
  // async loadUser() {
  //   await this.store.get('user').then((res) => {
  //     if (res) {
  //       this.user = res
  //     }
  //   })
  // }

  async doRefresh(event) {
    await this.loadReview()
    await event.target.complete();
  }

  async onReview() {
    const modal = await this.modalCtr.create({
      component: SendReviewComponent,
      backdropDismiss: true,
      cssClass: './review.component.scss'
    })

    modal.onWillDismiss().then((event: any) => {
      if (event.role == 'dismiss') {
        if (!event.data.error) {
          this.loadReview()
        }
      }
    })

    return await modal.present()
  }

  async deleteReview(r_id) {
    const alert = await this.alert.create({
      header: 'ลบรีวิวนี้',
      message: 'กด ยืนยัน เพื่อลบรีวิวนี้.',
      mode: 'ios',
      buttons: [
        {
          text: 'ยกเลิก',
          role: "cancel",
          cssClass: 'secondary',
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.api.post('deleteReview', { r_id: r_id }).subscribe(async (res) => {
              if (!res) {
                await this.api.Toast('ลบรีวิวเรียบร้อย')
                await setTimeout(() => {
                  this.loadReview()
                }, 1000);
              }
            })
          }
        }
      ]
    });

    await alert.present();
  }
}
