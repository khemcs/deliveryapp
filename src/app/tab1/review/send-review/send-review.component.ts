import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiServiceService } from 'src/app/api-service.service';
declare var liff: any;

@Component({
  selector: 'app-send-review',
  templateUrl: './send-review.component.html',
  styleUrls: ['./send-review.component.scss'],
})
export class SendReviewComponent implements OnInit {
  public localhost = window.location.href.indexOf("localhost") != -1;
  public comment: string = ''
  res = '';
  note = '';
  user: any = { mm_id: '' };
  constructor(private api: ApiServiceService, private store: Storage,
    public router: Router,
    private modalCtr: ModalController, private alert: AlertController) { }

  async ngOnInit() {
    await this.load_data()
  }


  async liff_init() {
    await liff.init({ liffId: this.data.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f"
      };
      this.api.post("update_profile", data).subscribe((res) => {
        console.log(res);
        this.user = res;
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {
      this.getUserProfile();
    } else {
      liff.login({ redirectUri: window.location.href })
    }
  }

  async getUserProfile() {
    const profile = await liff.getProfile();
    this.api.post("update_profile", profile).subscribe((res) => {
      this.user = res;

    });

  }

  public data: any = []
  load_data() {
    let url = this.router['routerState'].snapshot.url;
    let urls = url.split("/");
    this.res = urls[1];

    this.api.get("get_res/" + this.res).subscribe(async (res: any) => {
      this.data = await res.data;
      await this.liff_init()
    })
  }

  checkUploadFile(el) {
    if (this.src.length < 5) {
      el.click()
    } else {
      this.api.Toast('ไม่สามารถเพิ่มได้เกิน 5 รูป')
    }
  }

  public src: any = []
  changeFile(e) {
    var input = e.target
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (el) => {
        let temp = {
          path: el.target.result,
          name: input.files[0].name,
        }
        this.src.push(temp)
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    } else {
      //this.src = { path: '', name: 'โปรดแนบหลักฐานการโอน', state: false }
    }
  }

  cancelImg(i_of) {
    var temp: any = []
    this.src.forEach((el, index) => {
      if (index != i_of) {
        temp.push(el)
      }
    });
    this.src = temp
  }

  public starObjClass = ['', '', '', '', '',]
  public star = 0
  setStar(rate) {
    if (this.starObjClass[rate - 1] != '') {
      this.starObjClass = ['', '', '', '', '',]
      this.star = 0
      return false
    }
    let i = 0
    for (let s of [1, 2, 3, 4, 5]) {
      if (s <= rate) {
        this.starObjClass[i] = 'star-active'
      } else {
        this.starObjClass[i] = ''
      }
      i++
    }
    this.star = rate
  }

  async onPost() {
    var post = {
      user: this.user.mm_id,
      res: this.data.id_res_auto,
      src: this.src,
      star: this.star,
      comment: this.comment
    }

    if (post.star == 0 && post.comment == '') {
      const alert = await this.alert.create({
        mode: 'ios',
        header: 'ผิดผลาด!',
        subHeader: 'โปรดให้คะแนนรีวิวหรือแสดงความคิดเห็น.',
        buttons: ['ตกลง']
      });
      await alert.present();
    } else {
      this.api.post('postReview', post).subscribe(async (res) => {
        if (res) {
          await this.api.Toast('กำลังโพสต์รีวิวของท่านไปยังร้านอาหารนี้.')
          await this.modalCtr.dismiss({ error: false }, 'dismiss')
        }
      })
    }
  }


  close() {
    this.modalCtr.dismiss(undefined, 'close')
  }
}
