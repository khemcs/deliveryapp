import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { Tab1PageRoutingModule } from './tab1-routing.module';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { ReviewComponent } from './review/review.component';
import { GalleryComponent } from './gallery/gallery.component';
import { MoreComponent } from './more/more.component';
import { SendReviewComponent } from './review/send-review/send-review.component';
import { MyMapComponent } from '../my-map/my-map.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab1PageRoutingModule,
    //add: npm install --save ngx-ionic-image-viewer
    //imgae view
    NgxIonicImageViewerModule
  ],
  declarations: [Tab1Page,ReviewComponent,GalleryComponent,MoreComponent,SendReviewComponent,MyMapComponent],
  entryComponents : [SendReviewComponent,MyMapComponent]
})
export class Tab1PageModule {}
