import { MyMapComponent } from './../my-map/my-map.component';
import { Login2Page } from './../login2/login2.page';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AddReviewPage } from '../add-review/add-review.page';
import { ApiServiceService } from '../api-service.service';
import { Storage } from '@ionic/storage';
declare var liff: any;
declare var require: any
var moment = require('moment');
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public slideOpts = {
    initialSlides: 0,
    speed: 400
  }
  public select_tab = 1;
  public res: string = "";
  public localhost = window.location.href.indexOf("localhost") != -1 || window.location.href.indexOf("192.168.1") != -1;
  public data: any = {
    logo_url: '',
    name: '',
    review: '',
    address: '',
    img_bg_url: '',
    id_res_auto: '0',
    holiday: [],
    open_time: [],
    img_review_url: []
  };
  public reviews = [];
  public uri = '';
  public menu: any = {
    delivery: false,
    booking: false,
    queue: false,
    resive: false,
  }
  public img_default: string = 'assets/icon/logo_res_default.png'

  user: any = { mm_id: '' };
  constructor(public rounte: ActivatedRoute, public modalCtrl: ModalController,
    public rounter: Router, public modalController: ModalController,
    public api: ApiServiceService,
    private storage: Storage
  ) {
    this.res = this.rounte.snapshot.paramMap.get('res');
  }
  check_time(time, times) {
    let temp = { flag: false, at: '' };
    time = moment("2020-01-01 " + time).unix();
    times.forEach(element => {
      let time_start = moment("2020-01-01 " + element.time_start).unix()
      let time_end = moment("2020-01-01 " + element.time_end).unix()
      if (time_start <= time && time_end >= time) {
        temp.flag = true;
        temp.at = element.time_end;
      }
    });
    return temp;
  }

  check_date_time() {
    var is_open = true
    if (this.data.open_time.length == 0) {
      return { flag: false, at: 'ไม่มีกำหนด' }
    }
    let date = moment().format('YYYY-MM-DD')
    let temp = { flag: true, text: '', at: '' };
    this.data.holiday.forEach(element => {
      if (element.date == date) {
        temp.flag = false;
        temp.text = "วันหยุด " + element.text;
      }
    });
    // let day = moment().add(1, 'day').format('D') - 0;
    let day = moment().weekday() + 1;
    if (temp.flag) {
      let time = moment().format('HH:mm');
      if (this.data.open_time) {
        this.data.open_time.forEach(element => {
          if (element.day == 8) {
            temp.flag = this.check_time(time, element.time).flag;
            temp.at = this.check_time(time, element.time).at;
          } else if (element.day == 9 && (day == 2 || day == 3 || day == 4 || day == 5 || day == 6)) {
            temp.flag = this.check_time(time, element.time).flag;
            temp.at = this.check_time(time, element.time).at;
          } else if (element.day == 10 && (day == 7 || day == 1 || day == 2)) {
            temp.flag = this.check_time(time, element.time).flag;
            temp.at = this.check_time(time, element.time).at;
          } else if (element.day == day) {
            temp.flag = this.check_time(time, element.time).flag;
            temp.at = this.check_time(time, element.time).at;
          } else {
            temp.flag = false
          }
        });
      } else {
        temp.flag = false
      }
      if (!temp.flag) {
        temp.text = "ร้านไม่เปิดในวันที่หรือเวลาที่ท่านต้องการจอง";
      }
    }
    return temp;
  }

  async share(el) {
    el.open()
  }

  public share_val = 0
  changeShare() {
    var shares = [
      { key: 1, val: 'https://www.google.com/maps?q=loc:' + this.data.location },
      { key: 2, val: 'https://social-plugins.line.me/lineit/share?url=https://delivery.deltafood.me/' + this.res },
      { key: 3, val: 'https://www.facebook.com/sharer/sharer.php?u=https://delivery.deltafood.me/' + this.res },
      { key: 4, val: 'fb-messenger://share/?link=https://delivery.deltafood.me/' + this.res }
    ]
    shares.forEach(el => {
      if (this.share_val == el.key) {
        window.open(
          el.val,
          '_blank'
        );
      }
    });

  }

  tel() {
    window.location.href = "tel:" + this.data.tel;
  }

  map() {
    window.open(
      "https://www.google.com/maps?q=loc:" + this.data.location,
      '_blank' // <- This is what makes it open in a new window.
    );
    //  window.location.href = "https://www.google.com/maps?q=loc:"+this.data.location;
  }

  async liff_init() {
    await liff.init({ liffId: this.data.line_settings.liff_delivery });
    if (this.localhost) {
      var data = {
        'displayName': "น้องท็อป",
        'pictureUrl': "https://profile.line-scdn.net/0hXhpBX8mZB0dNOy-8foZ4EHF-CSo6FQEPNVtIdWs6WnAwX0gXcg9KKG0yUH9kWUgRcllOIW4_XSBh",
        'statusMessage': "โปรแกรมร้านอาหาร",
        'userId': "U20c8e632bc7eb10fa541ab685cf5ee5f",
        'id_res_auto': this.data.id_res_auto
      };
      this.api.post("update_profile", data).subscribe((res) => {
        this.user = res;
      });
    } else if (liff.isInClient() || liff.isLoggedIn()) {

      this.getUserProfile();
    } else {
      liff.login({ redirectUri: window.location.href })
    }
  }

  async getUserProfile() {
    const profile = await liff.getProfile();
    profile.id_res_auto = this.data.id_res_auto;
    this.api.post("update_profile", profile).subscribe((res) => {
      this.user = res;
    });
  }
  ngOnInit() {
    this.hasMap()
    setTimeout(() => {
      this.load_data();
    }, 1000);

  }

  // loadReview() {
  //   this.api.get('get_review/' + this.data.id_res_auto).subscribe((resr: any) => {
  //     this.reviews = resr;
  //     console.log(resr);
  //   })
  // }

  async markMap() {
    const modal = await this.modalCtrl.create({
      component: MyMapComponent,
      componentProps: {
        data: ''
      },
      backdropDismiss: false,
      cssClass: 'location_page',
    });

    modal.onWillDismiss().then((e: any) => {
      if (e.role) {
        this.api.setStore('my_pos', e.data)
      }
    })

    return await modal.present();
  }

  hasMap() {
    setTimeout(async () => {
      let isPos = this.api.getStore('my_pos')
      if (!isPos) {
        const modal = await this.modalCtrl.create({
          component: MyMapComponent,
          componentProps: {
            data: ''
          },
          backdropDismiss: false,
          cssClass: 'location_page',
        });

        modal.onWillDismiss().then((e: any) => {
          if (e.role) {
            this.api.setStore('my_pos', e.data)
          } else {
            let has_pos = this.api.getStore('my_pos')
            if (!has_pos) {
              this.hasMap()
            }
          }
        })
        return await modal.present();
      }

    }, 500);
  }

  async load_data() {
    let restaurant = this.api.getStore('restaurant')
    this.data = await restaurant
    await this.liff_init();
    await this.checkMenu()
    await this.api.get("get_res/" + this.res).subscribe((res: any) => {
      this.api.setStore('restaurant', res.data);
      this.data = res.data;
      // this.liff_init();
      // // this.loadReview();
      // this.checkMenu()
      // console.log(res.data);

    })
  }

  async rounter_url(uri) {
    if (this.user.mm_id != '') {
      await this.api.setStore('cart_restaurant_menu', uri)
      await this.rounter.navigate(['/' + this.res + "/" + uri]);
    } else {
      liff.login({ redirectUri: window.location.href })
    }
    // this.storage.get("login").then((val) => {
    //   if (val) {
    //     this.rounter.navigate(['/' + this.res + "/" + uri]);
    //   } else {
    //     this.uri = uri;
    //     this.login();
    //   }
    // });
  }

  checkMenu() {
    this.menu = this.data.btn
  }

  async login() {
    const modal = await this.modalCtrl.create({
      component: Login2Page,
      componentProps: { data: {} },
      backdropDismiss: false,
      cssClass: 'login_page',
    });

    modal.onWillDismiss().then((event: any) => {
      if (event.data.action) {
        this.rounter.navigate(['/' + this.res + "/" + this.uri]);
      }

    });

    return await modal.present();
  }

  async add_review(star) {

    const modal = await this.modalCtrl.create({
      component: AddReviewPage,
      componentProps: {
        star: star
      },
      backdropDismiss: false,
      cssClass: 'add_reviewpage',
    });

    modal.onWillDismiss().then((event: any) => {

    })

    return await modal.present();
  }

  mode(event) {
    let systemDark = window.matchMedia("(prefers-color-scheme: dark)");
    systemDark.addListener(this.colorTest);
    if (event.detail.checked) {
      document.body.setAttribute('data-theme', 'dark');
    }
    else {
      document.body.setAttribute('data-theme', 'light');
    }
  }

  colorTest(systemInitiatedDark) {
    if (systemInitiatedDark.matches) {
      document.body.setAttribute('data-theme', 'dark');
    } else {
      document.body.setAttribute('data-theme', 'light');
    }
  }
}
